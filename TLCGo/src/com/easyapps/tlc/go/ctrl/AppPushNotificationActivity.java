package com.easyapps.tlc.go.ctrl;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

public class AppPushNotificationActivity extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
		try {
			try {
			    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
			    AppPushNotificationActivity.this.finish();
			} catch (android.content.ActivityNotFoundException anfe) {
			    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
			    AppPushNotificationActivity.this.finish();
			}
		} catch (NullPointerException e) {
			if (e != null) {
				Log.w(" -->DEBUG", e);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w(" -->DEBUG", e);
			}
		}
	}
}