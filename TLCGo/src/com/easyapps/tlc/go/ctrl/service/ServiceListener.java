package com.easyapps.tlc.go.ctrl.service;

public interface ServiceListener {
	public void onServiceComplete(Object response, int eventType);
}
