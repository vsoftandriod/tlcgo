package com.easyapps.tlc.go.ctrl.service;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.util.Log;

import com.easyapps.tlc.go.ctrl.model.Category;
import com.easyapps.tlc.go.ctrl.model.NoticeBoard;
import com.easyapps.tlc.go.ctrl.model.NoticeId;
import com.easyapps.tlc.go.ctrl.model.Product;
import com.easyapps.tlc.go.ctrl.model.ProductAddress;
import com.easyapps.tlc.go.ctrl.model.ProductDetails;
import com.easyapps.tlc.go.ctrl.model.User;
import com.easyapps.tlc.go.ctrl.utils.ApplicationConstants;
import com.easyapps.tlc.go.ctrl.utils.Utility;
import com.easyapps.tlc.go.ctrl.xml.CategoryListParser;
import com.easyapps.tlc.go.ctrl.xml.DailyDealsParser;
import com.easyapps.tlc.go.ctrl.xml.HotOffersParser;
import com.easyapps.tlc.go.ctrl.xml.NoticeBoardParser;
import com.easyapps.tlc.go.ctrl.xml.NoticeboardIdParser;
import com.easyapps.tlc.go.ctrl.xml.ProductAddressesParser;
import com.easyapps.tlc.go.ctrl.xml.ProductDetailsParser;
import com.easyapps.tlc.go.ctrl.xml.UserDetailsParser;

@SuppressWarnings("unused")
public class TlcGoService implements NetworkListener {
	private static final int REQUEST_LOGIN = 1;
	private static final int REQUEST_USER_DETAILS = 2;
	private static final int REQUEST_CLIENT_BANNER = 3;
	private static final int REQUEST_CATEGORY_LIST = 4;
	private static final int REQUEST_HOT_OFFERS = 5;
	private static final int REQUEST_SEARCH_PRODUCTS = 6;
	private static final int REQUEST_PRODUCTS_DETAILS = 7;
	private static final int REQUEST_NEAREST_LAT_LON = 8;
	private static final int REQUEST_PRODUCTS_ADDRESSES = 9;
	private static final int REQUEST_REDEEM = 10;
	private static final int REQUEST_HELP_DETAILS = 11;
	private static final int REQUEST_NOTICE_BOARD = 12;
	private static final int REQUEST_DAILY_DEALS = 13;
	private static final int REQUEST_FIRSTTIME_LOGIN = 14;
	private static final int REQUEST_FIRST_TIME_USER_URL = 15;
	private static final int REQUEST_NOTICEBOARD_ID = 16;

	private ServiceListener serviceListener;
	private static TlcGoService tlcGoService;
	private String username;

	public static TlcGoService getTlcGoService() {
		if (tlcGoService == null) {
			tlcGoService = new TlcGoService();
		}
		return tlcGoService;
	}

	public void sendLoginRequest(ServiceListener serviceListener,
			String username, String password, String affiliateId) {
		try {
			this.serviceListener = serviceListener;
			this.username = username;
			HttpClient.getWWDispatchHandler().sendRequestAsync(ApplicationConstants.LOGIN_WRAPPER, getLoginRequestData(username, password, affiliateId), null, this, REQUEST_LOGIN);
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("-->DEBUG", e);
			}
		}
	}

	private String getLoginRequestData(String username, String password,
			String subDomainURL) {
		String loginXML = null;
		try {
			loginXML = "uname=" + username + "&pwd=" + password + "&sub="+ subDomainURL;
			return loginXML;
		} catch (Exception e) {
			return loginXML;
		}
	}

	private void sendUserDetailsRequest() {
		//http://www.myrewards.com.au/app/webroot/newapp/get_user.php?uname=sasi
		HttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.USER_DETAILS_WRAPPER + username, null,
				null, this, REQUEST_USER_DETAILS);
	}

	public void sendCategoriesListRequest(ServiceListener serviceListener) {
		this.serviceListener = serviceListener;
		if (Utility.user != null) {
			//http://www.myrewards.com.au/app/webroot/newapp/get_cat.php?&cid=24&country=Australia
			HttpClient.getWWDispatchHandler().sendRequestAsync(ApplicationConstants.CATEGORY_WRAPPER + "&cid="+ Utility.user.getClient_id() + "&country="+ Utility.user.getCountry(), null, null, this, REQUEST_CATEGORY_LIST);
			Log.w("-->", ApplicationConstants.CATEGORY_WRAPPER + "&cid="+ Utility.user.getClient_id() + "&country="+ Utility.user.getCountry());
		}
	}

	public void sendHotOffersRequest(ServiceListener serviceListener) {
		this.serviceListener = serviceListener;
		//http://www.myrewards.com.au/app/webroot/newapp/get_hot_offer.php?cid=24
		HttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.HOT_OFFERS_WRAPPER	+ Utility.user.getClient_id(), null, null, this,
				REQUEST_HOT_OFFERS);
	}
	public void sendTicketBoxRequest(ServiceListener serviceListener) {
		this.serviceListener = serviceListener;
		//http://www.myrewards.com.au/app/webroot/newapp/get_hot_offer.php?cid=24
		HttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.SEARCH_PRODUCTS_WRAPPER+"cat_id=396&cid="+Utility.user.getClient_id()+"&country="+Utility.user.getCountry()+"&start=0&limit=30", null, null, this,
				REQUEST_HOT_OFFERS);
	}
	public void sendSearchProductsRequest(ServiceListener serviceListener,
			String catID, String location, String keyword, int start, int limit) {
		this.serviceListener = serviceListener;
		String queryString = "cid=" + Utility.user.getClient_id();
		if (catID != null) {
			queryString = queryString + "&cat_id=" + catID;
		}
		if (location != null && location.length() > 0) {
			queryString = queryString + "&p=" + location;
		}
		if (keyword != null && keyword.length() > 0) {
			queryString = queryString + "&q=" + keyword;
		}
		String url = ApplicationConstants.SEARCH_PRODUCTS_WRAPPER + queryString+ "&country="+Utility.user.getCountry()+"&start=" + start + "&limit="+ limit;
		HttpClient.getWWDispatchHandler().sendRequestAsync(url, null, null, this, REQUEST_SEARCH_PRODUCTS);
		Log.w("-->", url);
		// url=http://www.myrewards.com.au/app/webroot/newapp/search.php?cid=24&cat_id=13&p=Sydney&q=food&country=Australia&start=0&limit=30
	}

	public void sendNearestLatLonRequest(ServiceListener serviceListener,
			Double lat, Double lon) {
		this.serviceListener = serviceListener;
		String queryString = "lat=" + lat + "&lng=" + lon + "&cid="
				+ Utility.user.getClient_id() + "&b=0.1" + "&c="+Utility.user.getCountry();
		//http: // java-ide-droid.googlecode.com/svn/trunk/ java-ide-droid

		// String queryString = "cid="+Utility.user.getClient_id();
		// queryString=queryString+"&lng="+lon+"&lat="+lat;
		
		//http://www.myrewards.com.au/app/webroot/newapp/get_products_by_loc.php?lat=17.4693768&lng=78.5092152&cid=24&b=0.1&c=Australia

		HttpClient.getWWDispatchHandler().sendRequestAsync(ApplicationConstants.NEAREST_LAT_LON_WRAPPER + queryString, null, null, this, REQUEST_NEAREST_LAT_LON);
	}

	public void sendProductDetailsRequest(ServiceListener serviceListener,
			int productId) {
		this.serviceListener = serviceListener;
		//http://www.myrewards.com.au/app/webroot/newapp/get_product.php?id=1000260
		String urls = ApplicationConstants.PRODUCT_DETAILS_WRAPPER + "id="+ productId;
		HttpClient.getWWDispatchHandler().sendRequestAsync(urls, null, null, this,REQUEST_PRODUCTS_DETAILS);
		Log.w("-->", urls);
	}

	public void sendProductAddresseRequest(ServiceListener serviceListener,
			int productId) {
		this.serviceListener = serviceListener;
		HttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.PRODUCT_ADDRESSES_WRAPPER + productId,
				null, null, this, REQUEST_PRODUCTS_ADDRESSES);
	}

	public void sendRedeemDetailsRequest(ServiceListener serviceListener,
			int productId) {
		this.serviceListener = serviceListener;
		HttpClient.getWWDispatchHandler().sendRequestAsync(ApplicationConstants.REDEEM_DETAILS_WRAPPER, "user_id=" + Utility.user.getId() + "&pid=" + productId	+ "&cid=" + Utility.user.getClient_id()+"&lat=0.0"+"&lon=0.0", null, this, REQUEST_REDEEM);
	}

	public void sendHelpRequest(ServiceListener serviceListener) {
		this.serviceListener = serviceListener;
		HttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.HELP_SCREEN_WRAPPER, null, null, this,
				REQUEST_HELP_DETAILS);
	}

	public void sendNoticeBoardRequest(ServiceListener serviceListener) {
		this.serviceListener = serviceListener;
		HttpClient.getWWDispatchHandler().sendRequestAsync(
				// ApplicationConstants.NOTICE_BOARD_WRAPPER+Utility.user.getClient_id(),
				ApplicationConstants.NOTICE_BOARD_WRAPPER, null, null, this,
				REQUEST_NOTICE_BOARD);
	}

	public void sendDailyDealsRequest(ServiceListener serviceListener) {
		this.serviceListener = serviceListener;
		//http://www.myrewards.com.au/newapp/get_daily_deal.php?country=Australia&cid=24
		HttpClient.getWWDispatchHandler().sendRequestAsync(
		ApplicationConstants.DAILY_DEALS_WRAPPER+"country="+Utility.user.getCountry()+"&cid="+Utility.user.getClient_id(), null, null, this, REQUEST_DAILY_DEALS);
	}

	public void onRequestCompleted(String response, String errorString,
			int eventType) {
		switch (eventType) {
		case REQUEST_LOGIN:
			if (errorString == null && response != null) {
				try {
					DocumentBuilderFactory factory = DocumentBuilderFactory
							.newInstance();
					DocumentBuilder db = factory.newDocumentBuilder();
					InputSource inStream = new InputSource();
					inStream.setCharacterStream(new StringReader(response));
					Document doc = db.parse(inStream);

					String message = "status";
					NodeList messageId_nl = doc.getElementsByTagName("status");
					for (int i = 0; i < messageId_nl.getLength(); i++) {
						if (messageId_nl.item(i).getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
							org.w3c.dom.Element nameElement = (org.w3c.dom.Element) messageId_nl
									.item(i);
							message = nameElement.getFirstChild()
									.getNodeValue().trim();
						}
					}
					if (message != null && message.equalsIgnoreCase("SUCCESS")) {
						sendUserDetailsRequest();
					} else
						serviceListener.onServiceComplete(message, eventType);

				} catch (Exception e) {
					e.printStackTrace();
					// serviceListener.onServiceComplete(ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL);
				}
			} else if (errorString != null) {
				serviceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;
		case REQUEST_USER_DETAILS:
			if (errorString == null && response != null) {
				try {
					User user = new User();
					new UserDetailsParser().internalXMLParse(response, user);
					serviceListener.onServiceComplete(user, eventType);
				} catch (Exception e) {
					e.printStackTrace();
					serviceListener.onServiceComplete(
							ApplicationConstants.USERNAME_PASSWORD_FIELDS,
							eventType);
				}
			} else if (errorString != null) {
				serviceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;
		case REQUEST_CATEGORY_LIST:
			if (errorString == null && response != null) {
				try {
					List<Category> categoryList = new ArrayList<Category>();
					new CategoryListParser().internalXMLParse(response,	categoryList);
					System.out.println("category list size:   "+ categoryList.size());
					serviceListener.onServiceComplete(categoryList,	eventType);
				} catch (Exception e) {
					e.printStackTrace();
					serviceListener.onServiceComplete(ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL, eventType);
				}
			} else if (errorString != null) {
				serviceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;
		case REQUEST_HOT_OFFERS:
			if (errorString == null && response != null) {
				try {
					List<Product> productsList = new ArrayList<Product>();
					new HotOffersParser().internalXMLParse(response,
							productsList);
					System.out.println("category list size:   "
							+ productsList.size());
					serviceListener.onServiceComplete(productsList, eventType);
				} catch (Exception e) {
					e.printStackTrace();
					serviceListener
							.onServiceComplete(ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL, eventType);
				}
			} else if (errorString != null) {
				serviceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;
		case REQUEST_SEARCH_PRODUCTS:
			if (errorString == null && response != null) {
				try {
					List<Product> productsList = new ArrayList<Product>();
					new HotOffersParser().internalXMLParse(response,
							productsList);
					System.out.println("category list size:   "
							+ productsList.size());
					serviceListener.onServiceComplete(productsList, eventType);
				} catch (Exception e) {
					e.printStackTrace();
					serviceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				serviceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;
		case REQUEST_NEAREST_LAT_LON:
			if (errorString == null && response != null) {
				try {
					List<Product> productsList = new ArrayList<Product>();
					new HotOffersParser().internalXMLParse(response,
							productsList);
					System.out.println("category list size:   "
							+ productsList.size());
					serviceListener.onServiceComplete(productsList, eventType);
				} catch (Exception e) {
					e.printStackTrace();
					serviceListener.onServiceComplete(ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL, eventType);
				}
			} else if (errorString != null) {
				serviceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;
		case REQUEST_PRODUCTS_DETAILS:
			if (errorString == null && response != null) {
				try {
					ProductDetails productDetails = new ProductDetails();
					new ProductDetailsParser().internalXMLParse(response,
							productDetails);
					serviceListener
							.onServiceComplete(productDetails, eventType);
				} catch (Exception e) {
					e.printStackTrace();
					serviceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				serviceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;
		case REQUEST_PRODUCTS_ADDRESSES:
			if (errorString == null && response != null) {
				try {
					List<ProductAddress> productAddressList = new ArrayList<ProductAddress>();
					new ProductAddressesParser().internalXMLParse(response,
							productAddressList);
					serviceListener.onServiceComplete(productAddressList,
							eventType);
				} catch (Exception e) {
					e.printStackTrace();
					serviceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				serviceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;
		case REQUEST_REDEEM:
			if (errorString == null && response != null) {
				try {
					serviceListener.onServiceComplete(response, eventType);
				} catch (Exception e) {
					e.printStackTrace();
					serviceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				serviceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;
		case REQUEST_HELP_DETAILS:
			if (errorString == null && response != null) {
				try {
					serviceListener.onServiceComplete(response, eventType);
				} catch (Exception e) {
					e.printStackTrace();
					serviceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				serviceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;
		case REQUEST_NOTICE_BOARD:
			if (errorString == null && response != null) {
				try {
					List<NoticeBoard> productsList = new ArrayList<NoticeBoard>();
					new NoticeBoardParser().internalXMLParse(response, productsList);
					System.out.println("category list size:   "	+ productsList.size());
					serviceListener.onServiceComplete(productsList, eventType);
				} catch (Exception e) {
					e.printStackTrace();
					serviceListener.onServiceComplete(ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL, eventType);
				}
			} else if (errorString != null) {
				serviceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;
		case REQUEST_DAILY_DEALS:
			if (errorString == null && response != null) {
				try {
					Product product = new Product();
					new DailyDealsParser().internalXMLParse(response, product);
					System.out.println("category list size:   " + product);
					serviceListener.onServiceComplete(product, eventType);
				} catch (Exception e) {
					e.printStackTrace();
					serviceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				serviceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;

		
		case REQUEST_NOTICEBOARD_ID:
			if (errorString == null && response != null) {
				List<NoticeId> noticeid = new ArrayList<NoticeId>();

				new NoticeboardIdParser().IdParser(response, noticeid);
				serviceListener.onServiceComplete(noticeid, eventType);

			}
			break;
		}
	}

	

	
	

	

	public void sendNoticeBoardCountIdRequset(ServiceListener serviceListener) {
		this.serviceListener = serviceListener;
		HttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.NOTICEBOARD_ID_URL, null, null, this,
				REQUEST_NOTICEBOARD_ID);
		// TODO Auto-generated method stub

	}
}
