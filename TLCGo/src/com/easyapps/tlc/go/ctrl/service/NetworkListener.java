package com.easyapps.tlc.go.ctrl.service;

public interface NetworkListener {
	void onRequestCompleted(String response, String errorString, int eventType);

}
