package com.easyapps.tlc.go.ctrl;

import org.jsoup.Jsoup;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.TextView;

import com.easyapps.tlc.go.ctrl.utils.Utility;

/*this is launcher splash screen*/

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
@SuppressLint({ "HandlerLeak", "ShowToast" })
public class SplashScreenActivity<TextProgressBar> extends Activity implements
		AnimationListener {

	// stopping splash screen starting home activity.
	private static final int STOPSPLASH = 0;
	// time duration in millisecond for which your splash screen should visible to
	// user. here i have taken half second
	private static final long SPLASHTIME = 2500;
	
	final private static int UPDATE_AVAILABLE = 1;
	final private static int NO_INTERNET = 2;
	
	//Alert Dialog Members variables 
	private TextView alertTitleTV, alertMessageTV;
	private Button yesBtn, noBtn;
	private String alertTitleStr, alertMsgStr, alertYesStr, alertNoStr;

	Integer currentAPILevel;
	// handler for splash screen
	private Handler splashHandler = new Handler() {
		
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case STOPSPLASH:
				// Generating and Starting new intent on splash time out
				boolean newVersion = false;
				if (Utility.isOnline((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE))) {
					if (!(currentAPILevel > 8)) {
						Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
						startActivity(intent);
						SplashScreenActivity.this.finish();
					} else {
						newVersion = web_update();
						if (newVersion == true) {
							alertTitleStr = getResources().getString(R.string.update_available_text);
							alertMsgStr = getResources().getString(R.string.update_version_message);
							alertYesStr = getResources().getString(R.string.yes_one);
							alertNoStr = getResources().getString(R.string.no_one);
							showDialog(UPDATE_AVAILABLE);
						} else {
							Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
							startActivity(intent);
							SplashScreenActivity.this.finish();
						}
					}
				} else {
					alertTitleStr = getResources().getString(R.string.alert_title_no_network_ava);
					alertMsgStr = getResources().getString(R.string.no_network_avail);
					alertYesStr = getResources().getString(R.string.ok_one);
					alertNoStr = null;
					showDialog(NO_INTERNET);
				}
				break;
			}
			super.handleMessage(msg);
		}

		private boolean web_update() {
			try {
				String package_name = getPackageName();
				String curVersion = getApplicationContext().getPackageManager()
						.getPackageInfo(package_name, 0).versionName;
				String newVersion = curVersion;
				newVersion = Jsoup
						.connect(
								"https://play.google.com/store/apps/details?id="
										+ package_name + "&hl=en")
						.timeout(30000)
						.userAgent(
								"Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
						.referrer("http://www.google.com").get()
						.select("div[itemprop=softwareVersion]").first()
						.ownText();
				return (value(curVersion) < value(newVersion)) ? true : false;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}

		private long value(String string) {
			string = string.trim();
			if (string.contains(".")) {
				final int index = string.lastIndexOf(".");
				return value(string.substring(0, index)) * 100
						+ value(string.substring(index + 1));
			} else {
				return Long.valueOf(string);
			}
		}
	};

	@SuppressLint("HandlerLeak")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.splashscreen);
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("-->", e);
			}
		}

		try {
			currentAPILevel = Integer.valueOf(android.os.Build.VERSION.SDK);
			if (currentAPILevel >= 11) {
				StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
				StrictMode.setThreadPolicy(policy); 
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("-->", e);
			}
		}

		try {
			Message msg = new Message();
			msg.what = STOPSPLASH;
			splashHandler.sendMessageDelayed(msg, SPLASHTIME);
		} catch (NoSuchFieldError e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("-->", e);
			}
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		System.gc();
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			AlertDialog alertDialog = null;
			LayoutInflater liYes = LayoutInflater.from(this);
			View callAddressView = liYes.inflate(R.layout.alert_all_in_one_dialog, null);
			AlertDialog.Builder adbrok = new AlertDialog.Builder(this);
			adbrok.setCancelable(false);
			adbrok.setView(callAddressView);
			alertDialog = adbrok.create();
			alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
			alertDialog.show();
			return alertDialog;
		} catch (Exception e) {
			if (e != null) {
				Log.w("-->", e);
			}
			return null;
		}
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		try {
			final AlertDialog Alert = (AlertDialog) dialog;
			alertTitleTV = (TextView) Alert.findViewById(R.id.alertYNTitleTVID);
			alertTitleTV.setTypeface(Utility.font_bold);
			alertTitleTV.setText(""+alertTitleStr);
			
			alertMessageTV = (TextView) Alert.findViewById(R.id.alertYNMessageTVID);
			alertMessageTV.setTypeface(Utility.font_reg);
			alertMessageTV.setText(""+alertMsgStr);
			
			yesBtn = (Button) Alert.findViewById(R.id.alertYESBtnID);
			yesBtn.setTypeface(Utility.font_bold);
			yesBtn.setText(""+alertYesStr);
			
			noBtn = (Button) Alert.findViewById(R.id.alertNOBtnID);
			if (alertNoStr == null) {
				noBtn.setVisibility(View.GONE);
			} else {
				noBtn.setTypeface(Utility.font_bold);
				noBtn.setText(""+alertNoStr);
			}
			Alert.setCancelable(false);
			yesBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (alertNoStr == null) {
						SplashScreenActivity.this.finish();
					} else {
						startActivity(new Intent(SplashScreenActivity.this, AppPushNotificationActivity.class));
						SplashScreenActivity.this.finish();
					}
					Alert.dismiss();
				}
			});
			noBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
					SplashScreenActivity.this.finish();
					Alert.dismiss();
				}
			});
		} catch (Exception e) {
			if (null != e) {
				Log.w("-->", e);
			}
		}
	}
	
	@Override
	public void onAnimationEnd(Animation arg0) {
		System.out.println("This is under animation starts ");
	}
	@Override
	public void onAnimationRepeat(Animation arg0) {
	}
	@Override
	public void onAnimationStart(Animation arg0) {
	}
}