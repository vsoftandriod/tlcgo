package com.easyapps.tlc.go.ctrl;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.easyapps.tlc.go.ctrl.model.Menu;
import com.easyapps.tlc.go.ctrl.utils.DatabaseHelper;
import com.easyapps.tlc.go.ctrl.utils.Utility;
import com.readystatesoftware.viewbadger.BadgeView;

/*
 * This is a BaseActivity for all the classes.the common functionalities like menu 
 * screen,header banner will be written here,so all the classes will extend this class
 * 
 * */


public class HomeBasedActivity extends FragmentActivity implements
		OnItemClickListener, OnClickListener {
	private final int SEARCH_BY_CATEGORIES = 0;
	private final int WHAT_IS_AROUNDME = 1;
	private final int UPCOMING_EVENTS = 2;
	protected final int MY_FAVOURITES = 3;
	private final int LATEST_PROMOTIONS = 4;
	private final int NOTICEBOARD = 5;
	private final int MY_MEMBERSHIP_CARD = 6;
	private final int BARCODE_SCANNER = 7;
	private final int PARKING_TIMER = 8;
	private final int FACEBOOK = 9;
	private final int TWITTER = 10;
	private final int HELP = 11;
	protected final int LOGOUT = 12;
	private int SELECTED_MENU_ITEM;
	public static boolean noticeboardcount = false;

	static int noticeCount = 0;
	// logout
	public TextView logoutText;
	public Button loginbutton1, cancelbutton1;

	LayoutInflater inflater;
	MenuListAdapter mAdapter;
	ArrayList<Menu> menuList;
	public ListView menuListView;
	public Button menuBtn;
	public static ImageView unreadImage;
	public ImageView noticeboardunread;
	int imagesList[] = { R.drawable.menu_search, R.drawable.menu_around_me, 
			R.drawable.menu_hot_offers, R.drawable.menu_my_favourites,
			R.drawable.menu_daily_deals, R.drawable.menu_notice_board,
			R.drawable.menu_membership_card, R.drawable.menu_scan_icon,
			R.drawable.menu_tickets, R.drawable.menu_facebook,
			R.drawable.menu_twitter, R.drawable.menu_help,
			R.drawable.menu_logout };
	public static String currentScreen = null;

	public static final String FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION = "com.easyapps.tlc.go.ctrl.FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION";
	private BaseActivityReceiver baseActivityReceiver = new BaseActivityReceiver();
	public static final IntentFilter INTENT_FILTER = createIntentFilter();

	// Alert Dialog Members variables
	public static TextView alertTitleTV, alertMessageTV;
	public static Button yesBtn, noBtn;
	public static String alertTitleStr, alertMsgStr, alertYesStr, alertNoStr;
	
	DatabaseHelper dbHelper;
	private static IntentFilter createIntentFilter() {
		IntentFilter filter = null;
		try {
			filter = new IntentFilter();
			filter.addAction(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION);
		} catch (Exception e) {
			filter = null;
			if (e != null) {
				Log.w("-->DEBUG", e);
			}
		}
		return filter;
	}

	protected void registerBaseActivityReceiver() {
		registerReceiver(baseActivityReceiver, INTENT_FILTER);
	}

	protected void unRegisterBaseActivityReceiver() {
		unregisterReceiver(baseActivityReceiver);
	}

	public class BaseActivityReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			try {
				if (intent.getAction().equals(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION)) {
					HomeBasedActivity.this.finish();
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("-->DEBUG", e);
				}
			}
		}
	}

	protected void closeAllActivities() {
		try {
			sendBroadcast(new Intent(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION));
			System.gc();
		} catch (Exception e) {
			if (e != null) {
				Log.w("-->DEBUG", e);
			}
		}
	}

	public void bannerVisibilty(boolean isVisible) {
		ImageView banner = (ImageView) findViewById(R.id.bannerIVID);
		
		
		
		banner.getLayoutParams().height = (int) (Utility.screenHeight / 11.0);
		// banner.getLayoutParams().height = Utility.screenHeight / 10;
		if (isVisible) {
			banner.setVisibility(View.VISIBLE);
		} else {
			banner.setVisibility(View.GONE);
		}
	}

	public void setHeaderTitle(String title) {
		TextView titleTV = (TextView) findViewById(R.id.titleTVID);
		titleTV.setTypeface(Utility.font_bold);
		titleTV.setText(title);
		currentScreen = title;
	}

	public void showBackButton() {
		Button backBtn = (Button) findViewById(R.id.backToSearchBtnID);
		backBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		backBtn.setVisibility(View.VISIBLE);
		backBtn.setOnClickListener(this);
	}

	public void initialiseViews() {
		try {
			menuList = new ArrayList<Menu>();
			for (int i = 0; i < imagesList.length; i++) {
				Menu menu = new Menu();
				menu.setImageResourceID(imagesList[i]);
				menu.setName(getResources().getStringArray(R.array.menuStrings)[i]);
				menuList.add(menu);
			}
			menuBtn = (Button) findViewById(R.id.menuBtnID);
			menuBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
			menuBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
			menuBtn.setOnClickListener(this);
			menuListView.setOnItemClickListener(this);
			mAdapter = new MenuListAdapter(this);
			menuListView.setAdapter(mAdapter);
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("-->DEBUG", e);
			}
		}
	}

	public class MenuListAdapter extends BaseAdapter {
		public MenuListAdapter(HomeBasedActivity menuListActivity) {
		}

		@Override
		public int getCount() {
			return menuList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(int pos, View view, ViewGroup arg2) {
			View menuListRow = null;
			if (menuListRow == null) {
				inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				menuListRow = (ViewGroup) inflater.inflate(
						R.layout.menu_list_item, null, false);
			}

			RelativeLayout menuRL = (RelativeLayout) menuListRow
					.findViewById(R.id.menuDeviceListRLID);
			menuRL.getLayoutParams().height = (int) (Utility.screenHeight / 13.8);
			TextView menuItemName = (TextView) menuListRow
					.findViewById(R.id.menuItemTVID);
			menuItemName.setTypeface(Utility.font_bold);
			menuItemName.setText(menuList.get(pos).getName());
			ImageView menuItemImage = (ImageView) menuListRow
					.findViewById(R.id.menuItemIVID);
			menuItemImage.getLayoutParams().width = (int) (Utility.screenWidth / 9);
			menuItemImage.getLayoutParams().height = (int) (Utility.screenHeight / 15.0);
			menuItemImage.setImageResource(menuList.get(pos)
					.getImageResourceID());

			if (pos == 5) {
				dbHelper = new DatabaseHelper(HomeBasedActivity.this);
				noticeboardunread = (ImageView) menuListRow
						.findViewById(R.id.menuItemUnreadIVID);
				BadgeView badge = new BadgeView(HomeBasedActivity.this,
						noticeboardunread);
				if (dbHelper.getNoticeDetails() != 0) {
					badge.setText(Integer.toString(dbHelper.getNoticeDetails()));
					badge.setBadgeBackgroundColor(Color.parseColor("#073B5F"));
					badge.setBadgePosition(BadgeView.POSITION_BOTTOM_LEFT);
					badge.show();
				} else {
					badge.setVisibility(View.GONE);
					noticeboardunread.setVisibility(View.GONE);
				}
				// }
			}

			View line = (View) menuListRow.findViewById(R.id.lineID);
			View lineBig = (View) menuListRow.findViewById(R.id.lineBigID);
			if (pos == 0)
				line.setVisibility(View.GONE);

			lineBig.setVisibility(View.GONE);
			line.getLayoutParams().width = 5 * (Utility.screenWidth / 6);

			if(pos==10)
			{
			menuRL.setVisibility(View.GONE);
			line.setVisibility(View.GONE);
			}
			
			
			return menuListRow;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View rowView, int pos,
			long arg3) {
		try {
			if (menuListView.getVisibility() == ListView.VISIBLE) {
				switch (pos) {
				case SEARCH_BY_CATEGORIES:
					try {
						if (Utility
								.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
							SELECTED_MENU_ITEM = SEARCH_BY_CATEGORIES;
							setForHeadersTitles(SELECTED_MENU_ITEM);
							if (SearchListActivity.headerTitle != null
									&& currentScreen != null) {
								if (currentScreen
										.equals(SearchListActivity.headerTitle)
										|| Utility.ResultScreen
												.equals("RESULT_LIST_SCREEN")) {
									if (menuListView.getVisibility() == View.VISIBLE) {
										applyMenuListSlideAnimation(0, -1);
										menuListView.setVisibility(View.GONE);
									}
								} else {
									Intent searchCategoriesIntent = new Intent(
											HomeBasedActivity.this,
											SearchListActivity.class);
									startActivity(searchCategoriesIntent);
								}
							} else {
								Intent searchCategoriesIntent = new Intent(
										HomeBasedActivity.this,
										SearchListActivity.class);
								startActivity(searchCategoriesIntent);
							}
						} else {
							showToast();
						}
					} catch (Exception e) {
						if (e != null) {
							e.printStackTrace();
							Log.w("-->DEBUG", e);
						}
					}
					break;
				case WHAT_IS_AROUNDME:// Revanth B00604 Tarun B00603
					try {
						if (Utility
								.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
							SELECTED_MENU_ITEM = WHAT_IS_AROUNDME;
							
							setForHeadersTitles(SELECTED_MENU_ITEM);
							if ((WhatsAroundMeActivity.headerTitle != null && currentScreen != null)) {
								if (currentScreen
										.equals(WhatsAroundMeActivity.headerTitle)
										|| Utility.AroundMeScreen
												.equals("AROUND_ME_SCREEN")) {
									if (menuListView.getVisibility() == View.VISIBLE) {
										applyMenuListSlideAnimation(0, -1);
										menuListView.setVisibility(View.GONE);
									}
								} else {
									Intent mapIntent = new Intent(
											HomeBasedActivity.this,
											WhatsAroundMeActivity.class);
									startActivity(mapIntent);
								}
							} else {
								Intent mapIntent = new Intent(
										HomeBasedActivity.this,
										WhatsAroundMeActivity.class);
								startActivity(mapIntent);
							}
						} else {
							showToast();
						}
					} catch (Exception e) {
						if (e != null) {
							e.printStackTrace();
							Log.w("-->DEBUG", e);
						}
					}
					break;
				case UPCOMING_EVENTS:
					try {
						if (Utility
								.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
							SELECTED_MENU_ITEM = UPCOMING_EVENTS;
						
							setForHeadersTitles(SELECTED_MENU_ITEM);
							if (UpcomingEventsActivity.headerTitle != null
									&& currentScreen != null) {
								if (currentScreen
										.equals(UpcomingEventsActivity.headerTitle)
										|| Utility.hotOffersScreen
												.equals("UPCOMING_EVENTS_SCREEN")) {
									if (menuListView.getVisibility() == View.VISIBLE) {
										applyMenuListSlideAnimation(0, -1);
										menuListView.setVisibility(View.GONE);
									}
								} else {
									Intent hotOffersIntent = new Intent(
											HomeBasedActivity.this,
											UpcomingEventsActivity.class);
									startActivity(hotOffersIntent);
								}
							} else {
								Intent hotOffersIntent = new Intent(
										HomeBasedActivity.this,
										UpcomingEventsActivity.class);
								startActivity(hotOffersIntent);
							}
						} else {
							showToast();
						}
					} catch (Exception e) {
						if (e != null) {
							e.printStackTrace();
							Log.w("-->DEBUG", e);
						}
					}
					break;
				case MY_FAVOURITES:
					try {
						if (Utility
								.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
							SELECTED_MENU_ITEM = MY_FAVOURITES;
							setForHeadersTitles(SELECTED_MENU_ITEM);
							DatabaseHelper dbHelper = new DatabaseHelper(
									getBaseContext());
							if (dbHelper.getProductList().size() > 0) {
								if (FavoritesListActivity.headerTitle != null
										&& currentScreen != null) {
									if (currentScreen
											.equals(FavoritesListActivity.headerTitle)
											|| Utility.FavouritesScreen
													.equals("FAVOURITES_SCREEN")) {
										if (menuListView.getVisibility() == View.VISIBLE) {
											applyMenuListSlideAnimation(0, -1);
											menuListView
													.setVisibility(View.GONE);
										}
									} else {
										Intent myFavoritesIntent = new Intent(
												HomeBasedActivity.this,
												FavoritesListActivity.class);
										startActivity(myFavoritesIntent);
									}
								} else {
									Intent myFavoritesIntent = new Intent(
											HomeBasedActivity.this,
											FavoritesListActivity.class);
									startActivity(myFavoritesIntent);
								}
							} else {
								alertTitleStr = getResources().getString(
										R.string.no_fav_here);
								alertMsgStr = getResources().getString(
										R.string.no_fav_founded);
								alertYesStr = getResources().getString(
										R.string.ok_one);
								alertNoStr = null;
								showDialog(MY_FAVOURITES);
							}
						} else {
							showToast();
						}
					} catch (Exception e) {
						if (e != null) {
							e.printStackTrace();
							Log.w("-->DEBUG", e);
						}
					}
					break;
				case LATEST_PROMOTIONS:
					try {
						if (Utility
								.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
							SELECTED_MENU_ITEM = LATEST_PROMOTIONS;
							setForHeadersTitles(SELECTED_MENU_ITEM);
							if (LatestPromotionsActivity.headerTitle != null
									&& currentScreen != null) {
								if (currentScreen
										.equals(LatestPromotionsActivity.headerTitle)
										|| Utility.DailyDealsScreen
												.equals("DAIYLY_DEALS_SCREEN")) {
									if (menuListView.getVisibility() == View.VISIBLE) {
										applyMenuListSlideAnimation(0, -1);
										menuListView.setVisibility(View.GONE);
									}
								} else {
									Intent dailyDealsIntent = new Intent(
											HomeBasedActivity.this,
											LatestPromotionsActivity.class);
									startActivity(dailyDealsIntent);
								}
							} else {
								Intent dailyDealsIntent = new Intent(
										HomeBasedActivity.this,
										LatestPromotionsActivity.class);
								startActivity(dailyDealsIntent);
							}
						} else {
							showToast();
						}
					} catch (Exception e) {
						if (e != null) {
							Log.w("-->DEBUG", e);
						}
					}
					break;
				case NOTICEBOARD:
					try {
						if (Utility
								.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
							noticeboardcount = true;
							if (noticeCount <= 1) {
								noticeCount = 1;
							}
							SELECTED_MENU_ITEM = NOTICEBOARD;
							setForHeadersTitles(SELECTED_MENU_ITEM);
							if (NoticeBoardActivity.headerTitle != null
									&& currentScreen != null) {
								if (currentScreen
										.equals(NoticeBoardActivity.headerTitle)) {
									if (menuListView.getVisibility() == View.VISIBLE) {
										applyMenuListSlideAnimation(0, -1);
										menuListView.setVisibility(View.GONE);
									}
								} else {
									Intent noticeBoardIntent = new Intent(
											HomeBasedActivity.this,
											NoticeBoardActivity.class);
									noticeBoardIntent.putExtra(
											"NOTICE_SPECIAL_COUNT", 3000);
									startActivity(noticeBoardIntent);
								}
							} else {
								Intent noticeBoardIntent = new Intent(
										HomeBasedActivity.this,
										NoticeBoardActivity.class);
								noticeBoardIntent.putExtra(
										"NOTICE_SPECIAL_COUNT", 3000);
								startActivity(noticeBoardIntent);
							}
						} else {
							showToast();
						}
					} catch (Exception e) {
						if (e != null) {
							Log.w("-->DEBUG", e);
						}
					}
					break;
				case HELP:
					try {
						if (Utility
								.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
							SELECTED_MENU_ITEM = HELP;
							setForHeadersTitles(SELECTED_MENU_ITEM);
							if (HelpPagesActivity.headerTitle != null
									&& currentScreen != null) {
								if (currentScreen
										.equals(HelpPagesActivity.headerTitle)) {
									if (menuListView.getVisibility() == View.VISIBLE) {
										applyMenuListSlideAnimation(0, -1);
										menuListView.setVisibility(View.GONE);
									}
								} else {
									Intent helpIntent = new Intent(
											HomeBasedActivity.this,
											HelpPagesActivity.class);
									startActivity(helpIntent);
								}
							} else {
								Intent helpIntent = new Intent(
										HomeBasedActivity.this,
										HelpPagesActivity.class);
								startActivity(helpIntent);
							}
						} else {
							showToast();
						}
					} catch (Exception e) {
						if (e != null) {
							Log.w("-->DEBUG", e);
						}
					}
					break;
				case MY_MEMBERSHIP_CARD:
					try {

						if (Utility
								.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
							SELECTED_MENU_ITEM = MY_MEMBERSHIP_CARD;
							setForHeadersTitles(SELECTED_MENU_ITEM);
							if (MyMemberShipCardActivity.headerTitle != null
									&& currentScreen != null) {
								if (currentScreen
										.equals(MyMemberShipCardActivity.headerTitle)) {
									if (menuListView.getVisibility() == View.VISIBLE) {
										applyMenuListSlideAnimation(0, -1);
										menuListView.setVisibility(View.GONE);
									}
								} else {
									Intent membershipCardIntent = new Intent(
											HomeBasedActivity.this,
											MyMemberShipCardActivity.class);
									startActivity(membershipCardIntent);
								}
							} else {
								Intent membershipCardIntent = new Intent(
										HomeBasedActivity.this,
										MyMemberShipCardActivity.class);
								startActivity(membershipCardIntent);
							}
						} else {
							showToast();
						}
					} catch (Exception e) {
						if (e != null) {
							Log.w("-->DEBUG", e);
						}
					}
					break;
				case BARCODE_SCANNER:
					SELECTED_MENU_ITEM = BARCODE_SCANNER;
					Intent bar_code_scanner = new Intent(
							HomeBasedActivity.this,
							BarCodeScannerActivity.class);
					startActivity(bar_code_scanner);
					currentScreen = null;
					break;
				case PARKING_TIMER:
					try {
						SELECTED_MENU_ITEM = PARKING_TIMER;
						setForHeadersTitles(SELECTED_MENU_ITEM);
						if (TicketBoxOfficeActivity.headerTitle != null
								&& currentScreen != null) {
							if (currentScreen
									.equals(TicketBoxOfficeActivity.headerTitle) || Utility.ticketoffersScreen
									.equals("TICKET_OFFERS_SCREEN")) {
								if (menuListView.getVisibility() == View.VISIBLE) {
									applyMenuListSlideAnimation(0, -1);
									menuListView.setVisibility(View.GONE);
								}
							} else {
								Intent parking_timer = new Intent(
										HomeBasedActivity.this,
										TicketBoxOfficeActivity.class);
								startActivity(parking_timer);
							}
						} else {
							Intent parking_timer = new Intent(
									HomeBasedActivity.this,
									TicketBoxOfficeActivity.class);
							startActivity(parking_timer);
						}
					} catch (Exception e) {
						if (e != null) {
							Log.w("-->DEBUG", e);
						}
					}
					break;
				case FACEBOOK:
					try {
						if (Utility
								.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
							SELECTED_MENU_ITEM = FACEBOOK;
							Intent i = new Intent(Intent.ACTION_VIEW);
							i.setData(Uri
									.parse("https://www.facebook.com/MyRewardsHK"));
							startActivity(i);
							currentScreen = null;
						} else {

						}
					} catch (Exception e) {
						if (e != null) {
							Log.w("-->DEBUG", e);
						}
					}
					break;
				case TWITTER:
					try {
						if (Utility
								.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
							setForHeadersTitles(SELECTED_MENU_ITEM);
							Intent in = new Intent(Intent.ACTION_VIEW);
							in.setData(Uri
									.parse("https://twitter.com/MyRewardsIntl"));
							startActivity(in);
							currentScreen = null;
						} else {
							showToast();
						}
					} catch (Exception e) {
						if (e != null) {
							Log.w("-->DEBUG", e);
						}
					}
					break;
				case LOGOUT:
					SELECTED_MENU_ITEM = LOGOUT;
					alertTitleStr = getResources().getString(
							R.string.logout_ones);
					alertMsgStr = getResources().getString(
							R.string.logout_confirm);
					alertYesStr = getResources().getString(R.string.yes_one);
					alertNoStr = getResources().getString(R.string.no_one);
					showDialog(LOGOUT);
					break;
				default:
					break;
				}
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("-->DEBUG", e);
			}
		}
	}

	private void showToast() {
		// The Custom Toast Layout Imported here
		LayoutInflater inflater = getLayoutInflater();
		View layout = inflater.inflate(R.layout.toast_no_netowrk,
				(ViewGroup) findViewById(R.id.custom_toast_layout_id));

		// The actual toast generated here.
		Toast toast = new Toast(getApplicationContext());
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setView(layout);
		toast.show();
	}

	private void setForHeadersTitles(int sELECTED_MENU_ITEM2) {
		switch (sELECTED_MENU_ITEM2) {
		case SEARCH_BY_CATEGORIES:
			if (currentScreen != null) {
				if (!currentScreen.equals("Search")) {
					currentScreen = null;
				}
			}
			break;
		case WHAT_IS_AROUNDME:
			if (currentScreen != null) {
				if (currentScreen.equals("Search")) {
					currentScreen = null;
				}
			}
			break;
		case UPCOMING_EVENTS:
			if (currentScreen != null) {
				if (currentScreen.equals("Search")) {
					currentScreen = null;
				}
			}
			break;
		case MY_FAVOURITES:
			if (currentScreen != null) {
				if (currentScreen.equals("Search")) {
					currentScreen = null;
				}
			}
			break;
		case LATEST_PROMOTIONS:
			if (currentScreen != null) {
				if (currentScreen.equals("Search")) {
					currentScreen = null;
				}
			}
			break;
		case NOTICEBOARD:
			if (currentScreen != null) {
				if (currentScreen.equals("Search")) {
					currentScreen = null;
				}
			}
			break;
		case MY_MEMBERSHIP_CARD:
			if (currentScreen != null) {
				if (currentScreen.equals("Search")) {
					currentScreen = null;
				}
			}
			break;
		case PARKING_TIMER:
			if (currentScreen != null) {
				if (currentScreen.equals("Search")) {
					currentScreen = null;
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			AlertDialog alertDialog = null;
			LayoutInflater liYes = LayoutInflater.from(this);
			View callAddressView = liYes.inflate(
					R.layout.alert_all_in_one_dialog, null);
			AlertDialog.Builder adbrok = new AlertDialog.Builder(this);
			adbrok.setCancelable(false);
			adbrok.setView(callAddressView);
			alertDialog = adbrok.create();
			alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
			alertDialog.show();
			return alertDialog;
		} catch (Exception e) {
			if (e != null) {
				Log.w("-->", e);
			}
			return null;
		}
	}

	@Override
	protected void onPrepareDialog(final int id, Dialog dialog) {
		try {
			final AlertDialog alertDialog = (AlertDialog) dialog;
			alertTitleTV = (TextView) alertDialog
					.findViewById(R.id.alertYNTitleTVID);
			alertTitleTV.setTypeface(Utility.font_bold);
			alertTitleTV.setText("" + alertTitleStr);

			alertMessageTV = (TextView) alertDialog
					.findViewById(R.id.alertYNMessageTVID);
			alertMessageTV.setTypeface(Utility.font_reg);
			alertMessageTV.setText("" + alertMsgStr);

			yesBtn = (Button) alertDialog.findViewById(R.id.alertYESBtnID);
			yesBtn.setTypeface(Utility.font_bold);
			yesBtn.setText("" + alertYesStr);

			noBtn = (Button) alertDialog.findViewById(R.id.alertNOBtnID);
			if (alertNoStr == null) {
				noBtn.setVisibility(View.GONE);
			} else {
				noBtn.setTypeface(Utility.font_bold);
				noBtn.setText("" + alertNoStr);
			}
			alertDialog.setCancelable(false);
			yesBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (id == LOGOUT) {
						try {
							if (dbHelper != null) {
								dbHelper.deleteLoginDetails();
								closeAllActivities();
							}
						} catch (Exception e) {
							if (e != null) {
								Log.w("-->Debug", e);
							}
						}
						Intent logoutIntent = new Intent(
								HomeBasedActivity.this, LoginActivity.class);
						logoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_SINGLE_TOP);
						logoutIntent.putExtra("finish", true);
						startActivity(logoutIntent);
						LoginActivity.etUserId.setText("");
						LoginActivity.etPasswd.setText("");
						LoginActivity.subDomainURL_et.setText("");
						HomeBasedActivity.this.finish();
					}
					alertDialog.dismiss();
				}
			});
			noBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					alertDialog.dismiss();
				}
			});
		} catch (Exception e) {
			if (null != e) {
				Log.w("-->", e);
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.menuBtnID:
			if (menuListView.getVisibility() == View.GONE) {
				menuListView.setVisibility(View.VISIBLE);
				applyMenuListSlideAnimation(-1, 0);
			} else {
				applyMenuListSlideAnimation(0, -1);
				menuListView.setVisibility(View.GONE);
			}
			break;
		case R.id.backToSearchBtnID:
			Button backBtn = (Button) findViewById(R.id.backToSearchBtnID);
			backBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
			backBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
			backBtn.setVisibility(View.INVISIBLE);
			HomeBasedActivity.this.finish();
			break;
		}
	}

	public void applyMenuListSlideAnimation(float start, float end) {
		TranslateAnimation translate = new TranslateAnimation(
				TranslateAnimation.RELATIVE_TO_SELF, 0,
				TranslateAnimation.RELATIVE_TO_SELF, 0,
				TranslateAnimation.RELATIVE_TO_SELF, start,
				TranslateAnimation.RELATIVE_TO_SELF, end);
		translate.setDuration(500);
		translate.setStartOffset(500);
		menuListView.startAnimation(translate);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			HomeBasedActivity.this.finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
