package com.easyapps.tlc.go.ctrl;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.easyapps.tlc.go.ctrl.imagecache.SmartImageView;
import com.easyapps.tlc.go.ctrl.model.Product;
import com.easyapps.tlc.go.ctrl.service.TlcGoService;
import com.easyapps.tlc.go.ctrl.service.ServiceListener;
import com.easyapps.tlc.go.ctrl.utils.ApplicationConstants;
import com.easyapps.tlc.go.ctrl.utils.Utility;

/*this activity will show the ticket box office details in listview.
 * the list item will contain the product name and highlight info.
 * the list item click will redirect to the product details view.*/

public class TicketBoxOfficeActivity extends HomeBasedActivity implements ServiceListener {
	List<Product> hotOffersProductsList;
	HotOffersAdapter mAdapter;
	View loading;
	ListView hotOffersListView;
	public static String headerTitle = null;

	SmartImageView banner;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.results_list);
		registerBaseActivityReceiver();
		Utility.AroundMeScreen = "null";
		Utility.FavouritesScreen ="null";
		Utility.DailyDealsScreen ="null";
		Utility.ResultScreen ="null";
		
		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);
		
		headerTitle = getResources().getString(R.string.parking_title_text);
		setHeaderTitle(headerTitle);
		
		menuBtn = (Button) findViewById(R.id.menuBtnID);
		menuBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		menuBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		menuListView = (ListView) findViewById(R.id.menuListViewID);
		initialiseViews();
		loading = (View) findViewById(R.id.loading);
		hotOffersListView = (ListView) findViewById(R.id.resultsListViewID);
		hotOffersProductsList = new ArrayList<Product>();
		hotOffersListView.setOnItemClickListener(this);
			
		//this is for banner
		
		 banner = (SmartImageView) findViewById(R.id.bannerIVID);
		banner.getLayoutParams().height = (int) (Utility.screenHeight / 11.0);
		banner.getLayoutParams().width = Utility.screenWidth;
		
		if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			TlcGoService.getTlcGoService().sendTicketBoxRequest(this);
		} else {
			// The Custom Toast Layout Imported here
			LayoutInflater inflater = getLayoutInflater();
			View layout = inflater.inflate(R.layout.toast_no_netowrk,
			(ViewGroup) findViewById(R.id.custom_toast_layout_id));
						 
			// The actual toast generated here.
			Toast toast = new Toast(getApplicationContext());
			toast.setDuration(Toast.LENGTH_LONG);
			toast.setView(layout);
			toast.show();
			TicketBoxOfficeActivity.this.finish();
			headerTitle = null;
		}
	}
	
	@Override
	protected void onDestroy() {
	   super.onDestroy();
	   System.gc();
	   unRegisterBaseActivityReceiver();
	}

	public class HotOffersAdapter extends BaseAdapter {

		public HotOffersAdapter(TicketBoxOfficeActivity hotOffersActivity) {
		}

		@Override
		public int getCount() {
			return hotOffersProductsList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(int pos, View view, ViewGroup arg2) {
			try {
				View resultsListRow = null;
				if (resultsListRow == null) {
					inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					resultsListRow = (View) inflater.inflate(R.layout.results_list_item, null, false);
				}
				LinearLayout rowLL = (LinearLayout) resultsListRow.findViewById(R.id.resultItemLLID);
				LinearLayout rowLL2 = (LinearLayout)resultsListRow.findViewById(R.id.resultListItemLLID);
				rowLL2.getLayoutParams().height = (int) (Utility.screenHeight / 8.5);
				TextView productNameTV = (TextView) resultsListRow.findViewById(R.id.productTVID);
				productNameTV.setTypeface(Utility.font_bold);
				
				TextView highlightTV = (TextView) resultsListRow.findViewById(R.id.offerTVID);
				highlightTV.setTypeface(Utility.font_reg);
				
				productNameTV.setText(hotOffersProductsList.get(pos).getName());
				highlightTV.setText(hotOffersProductsList.get(pos).getHighlight());
				switch (pos % 4) {
				case 0:
					rowLL.setBackgroundResource(R.color.result_color_one);
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
					break;
				case 1:
					rowLL.setBackgroundResource(R.color.result_color_two);
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
					break;
				case 2:
					rowLL.setBackgroundResource(R.color.result_color_three);
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
					break;
				case 3:
					rowLL.setBackgroundResource(R.color.result_color_four);
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
					break;
				}
				return resultsListRow;
			
			} catch (Exception e) {
				if (e != null) {
					Log.w("-->", e);
				}
				return null;
			}
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View rowView, int pos,	long arg3) {
		super.onItemClick(arg0, rowView, pos, arg3);
		if (arg0.getId() == R.id.resultsListViewID) {
			try {//2131230975 //android.widget.ListView@42047278
				if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					if (menuListView.getVisibility() == ListView.GONE) {
						Intent detailsIntent = new Intent(TicketBoxOfficeActivity.this, ProductDetailsActivity.class);
						detailsIntent.putExtra(ApplicationConstants.PRODUCT_ID_KEY, hotOffersProductsList.get(pos).getId());
						detailsIntent.putExtra(ApplicationConstants.COLOR_CODE_KEY, pos % 4);
						detailsIntent.putExtra(ApplicationConstants.PRODUCT_NAME_KEY, hotOffersProductsList.get(pos).getName());
						detailsIntent.putExtra(ApplicationConstants.PRODUCT_HIGHLIGHT_KEY, hotOffersProductsList.get(pos).getHighlight());
						detailsIntent.putExtra(Utility.FINISHED_STATUS, "TICKET_OFFERS_SCREEN");
						startActivity(detailsIntent);
					}
				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater.inflate(R.layout.toast_no_netowrk,
					(ViewGroup) findViewById(R.id.custom_toast_layout_id));
								 
					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_LONG);
					toast.setView(layout);
					toast.show();
				}
			
			} catch (Exception e) {
				if (e != null) {
					Log.w("-->DEBUG", e);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onServiceComplete(Object response, int eventType) {
		try{
		if (response != null) {
			if (response instanceof String) {
			} else {
				if(eventType!=16 && eventType==5){
				hotOffersProductsList = (ArrayList<Product>) response;
				if (Utility.user.getClientBanner() == null) {
					String bannerURL = ApplicationConstants.CLIENT_BANNER_WRAPPER+ Utility.user.getClient_id();
					newImagesLoading(bannerURL);
				} 
				mAdapter = new HotOffersAdapter(this);
				hotOffersListView.setAdapter(mAdapter);
				loading.setVisibility(View.GONE);
				}
			}
		}
	}catch(Exception e)
		{
		if (e != null) {
			e.printStackTrace();
			Log.w("-->DEBUG", e);
		}
		}
	}
	
	private void newImagesLoading(String _bannerPath) {
	

		// Image url
        String image_url = _bannerPath;
        Log.w("-->", _bannerPath);
        
        try {
        	banner.setImageUrl(image_url);
		} catch (OutOfMemoryError e) {
			if (e != null) {
				Log.w("-->DEBUG", e);
			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			Intent i = new Intent(TicketBoxOfficeActivity.this, SearchListActivity.class);
			i.putExtra("finish", true);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
			startActivity(i);
			TicketBoxOfficeActivity.this.finish();
			headerTitle = null;
		}
		return super.onKeyDown(keyCode, event);
	}
}
