package com.easyapps.tlc.go.ctrl;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.easyapps.tlc.go.ctrl.imagecache.SmartImageView;
import com.easyapps.tlc.go.ctrl.model.Product;
import com.easyapps.tlc.go.ctrl.utils.ApplicationConstants;
import com.easyapps.tlc.go.ctrl.utils.DatabaseHelper;
import com.easyapps.tlc.go.ctrl.utils.Utility;

/*this activity will show the list of favourite products*/

public class FavoritesListActivity extends HomeBasedActivity {//implements IImageURLTaskListener {
	View loading;
	ListView favoritesListView;
	public static String headerTitle = null;
	public static final int DELETE_MY_FAVORITE_BUTTON = 1000;
	MyFavoritesAdapter myFavAdapter;
	TextView noFavouritesadded;
	List<Product> myFavoriteProductsList;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.results_list);
		// must assign these
		Utility.hotOffersScreen = "null";
		Utility.AroundMeScreen = "null";
		Utility.DailyDealsScreen ="null";
		Utility.ResultScreen ="null";
		Utility.ticketoffersScreen="null";
		registerBaseActivityReceiver();
		try {
			setUpViews();
		} catch (NullPointerException e) {
			if (e != null) {
				Log.w("-->DEBUG", e);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("-->DEBUG", e);
			}
		}
	}
	
	private void setUpViews() {
		headerTitle = getResources().getString(R.string.my_favourites_text);
		setHeaderTitle(headerTitle);
		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);
		loading = (View) findViewById(R.id.loading);
		loading.setVisibility(View.GONE);
		dbHelper = new DatabaseHelper(this);
		menuBtn = (Button) findViewById(R.id.menuBtnID);
		menuBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		menuBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		menuListView = (ListView) findViewById(R.id.menuListViewID);
		initialiseViews();

		favoritesListView = (ListView) findViewById(R.id.resultsListViewID);
		myFavoriteProductsList = new ArrayList<Product>();
		favoritesListView.setOnItemClickListener(this);		
		noFavouritesadded = (TextView) findViewById(R.id.noFavAdded_deleteTVID);		
		try {
			myFavoriteProductsList = dbHelper.getProductList();
			if (myFavoriteProductsList.size() == 0) {
				noFavouritesadded.setTypeface(Utility.font_bold);
				noFavouritesadded.setVisibility(View.VISIBLE);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("-->DEBUG", e);
			}
		}
		
		if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			if (Utility.user != null) {
				if (Utility.user.getClientBanner() == null) {
					String bannerURL = ApplicationConstants.CLIENT_BANNER_WRAPPER+ Utility.user.getClient_id();
					newImagesLoading(bannerURL);
				}
			}
		} else {
			// The Custom Toast Layout Imported here
			LayoutInflater inflater = getLayoutInflater();
			View layout = inflater.inflate(R.layout.toast_no_netowrk, (ViewGroup) findViewById(R.id.custom_toast_layout_id));

			// The actual toast generated here.
			Toast toast = new Toast(getApplicationContext());
			toast.setDuration(Toast.LENGTH_LONG);
			toast.setView(layout);
			toast.show();
		}
		
		myFavAdapter = new MyFavoritesAdapter(this);
		favoritesListView.setAdapter(myFavAdapter);
		favoritesListView.setOnItemClickListener(this);
	}
	
	@Override
		protected void onResume() {
			super.onResume();
			try {
				setUpViews();
			} catch (NullPointerException e) {
				if (e != null) {
					Log.w("-->DEBUG", e);
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("-->DEBUG", e);
				}
			}
		}

	@Override
	protected void onDestroy() {
	   super.onDestroy();
	   System.gc();
	   unRegisterBaseActivityReceiver();
	}
	
	private void newImagesLoading(String _bannerPath) {
		// Loader image - will be shown before loading image
		SmartImageView banner = (SmartImageView) findViewById(R.id.bannerIVID);
		banner.getLayoutParams().height = (int) (Utility.screenHeight / 11.0);
		banner.getLayoutParams().width = Utility.screenWidth;
		
		// Image url
        String image_url = _bannerPath;
        Log.w("-->", _bannerPath);

        try {
        	//imgLoader.DisplayImage(image_url, banner);
        	banner.setImageUrl(image_url);
		} catch (OutOfMemoryError e) {
			if (e != null) {
				Log.w("-->DEBUG", e);
			}
		}
	}

	public class MyFavoritesAdapter extends BaseAdapter {
		public MyFavoritesAdapter(FavoritesListActivity favoritesListActivity) {
		}

		@Override
		public int getCount() {
			return myFavoriteProductsList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(final int pos, View view, ViewGroup arg2) {
			try {
				View resultsListRow = null;
				if (resultsListRow == null) {
					inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					resultsListRow = (View) inflater.inflate(R.layout.my_favorites_list_item, null, false);
				}
				RelativeLayout rowLL = (RelativeLayout) resultsListRow.findViewById(R.id.favResultListItemLLID);
				RelativeLayout rowLL2 = (RelativeLayout) resultsListRow.findViewById(R.id.resultItemLLID);
				rowLL2.getLayoutParams().height = (int) (Utility.screenHeight / 8.0);
				TextView productNameTV = (TextView) resultsListRow.findViewById(R.id.productMyTVID);
				productNameTV.setTypeface(Utility.font_bold);
				TextView highlightTV = (TextView) resultsListRow.findViewById(R.id.offerMyTVID);
				highlightTV.setTypeface(Utility.font_reg);
				Button deleteBtn = (Button) resultsListRow.findViewById(R.id.deleteMyFavoriteBtnID);
				deleteBtn.setFocusable(false);
				deleteBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {

						if (v.getId() == R.id.deleteMyFavoriteBtnID) {
							Utility.fav_position_var = pos;
							alertTitleStr = getResources().getString(R.string.alert_title_remove_fav2);
							alertMsgStr = getResources().getString(R.string.alert_title_remove_fav2_test)
									+ " '"
									+ myFavoriteProductsList.get(Utility.fav_position_var).getName() + "' " + getResources().getString(R.string.remove_favourite_message2);
							alertYesStr = getResources().getString(R.string.yes_one);
							alertNoStr = getResources().getString(R.string.no_one);
							showDialog(DELETE_MY_FAVORITE_BUTTON);
						} else {
							if (menuListView.getVisibility() == ListView.GONE) {
								if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
									if (menuListView.getVisibility() == ListView.GONE) {
										Intent detailsIntent = new Intent(FavoritesListActivity.this,ProductDetailsActivity.class);
										detailsIntent.putExtra(ApplicationConstants.PRODUCT_ID_KEY,	myFavoriteProductsList.get(pos).getId());
										detailsIntent.putExtra(ApplicationConstants.COLOR_CODE_KEY,	pos % 4);
										detailsIntent.putExtra(ApplicationConstants.PRODUCT_NAME_KEY,myFavoriteProductsList.get(pos).getName());
										detailsIntent.putExtra(ApplicationConstants.PRODUCT_HIGHLIGHT_KEY, myFavoriteProductsList.get(pos).getHighlight());
										detailsIntent.putExtra(Utility.FINISHED_STATUS, "FAVOURITES_SCREEN");
										startActivity(detailsIntent);
									}
								} else {
									// The Custom Toast Layout Imported here
									LayoutInflater inflater = getLayoutInflater();
									View layout = inflater.inflate(R.layout.toast_no_netowrk, (ViewGroup) findViewById(R.id.custom_toast_layout_id));

									// The actual toast generated here.
									Toast toast = new Toast(getApplicationContext());
									toast.setDuration(Toast.LENGTH_LONG);
									toast.setView(layout);
									toast.show();
								}
							}
						}
					}
				});
				productNameTV.setText(myFavoriteProductsList.get(pos).getName());
				highlightTV.setText(myFavoriteProductsList.get(pos).getHighlight());
				switch (pos % 4) {
				case 0:
					rowLL.setBackgroundResource(R.color.result_color_one);
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
					break;
				case 1:
					rowLL.setBackgroundResource(R.color.result_color_two);
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
					break;
				case 2:
					rowLL.setBackgroundResource(R.color.result_color_three);
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
					break;
				case 3:
					rowLL.setBackgroundResource(R.color.result_color_four);
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
					break;
				}
				return resultsListRow;
			
			} catch (Exception e) {
				if (e != null) {
					Log.w("-->DEBUG", e);
				}
				return null;
			}
		}
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			AlertDialog alertDialog = null;
			LayoutInflater liYes = LayoutInflater.from(this);
			View callAddressView = liYes.inflate(R.layout.alert_all_in_one_dialog, null);
			AlertDialog.Builder adbrok = new AlertDialog.Builder(this);
			adbrok.setCancelable(false);
			adbrok.setView(callAddressView);
			alertDialog = adbrok.create();
			alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
			alertDialog.show();
			return alertDialog;
		} catch (Exception e) {
			if (e != null) {
				Log.w("-->", e);
			}
			return null;
		}
	}

	@Override
	protected void onPrepareDialog(final int id, Dialog dialog) {
		try {
			final AlertDialog alertDialog = (AlertDialog) dialog;
			alertTitleTV = (TextView) alertDialog.findViewById(R.id.alertYNTitleTVID);
			alertTitleTV.setTypeface(Utility.font_bold);
			alertTitleTV.setText(""+alertTitleStr);
			
			alertMessageTV = (TextView) alertDialog.findViewById(R.id.alertYNMessageTVID);
			alertMessageTV.setTypeface(Utility.font_reg);
			alertMessageTV.setText(""+alertMsgStr);
			
			yesBtn = (Button) alertDialog.findViewById(R.id.alertYESBtnID);
			yesBtn.setTypeface(Utility.font_bold);
			yesBtn.setText(""+alertYesStr);
			
			noBtn = (Button) alertDialog.findViewById(R.id.alertNOBtnID);
			if (alertNoStr == null) {
				noBtn.setVisibility(View.GONE);
			} else {
				noBtn.setTypeface(Utility.font_bold);
				noBtn.setText(""+alertNoStr);
			}
			alertDialog.setCancelable(false);
			yesBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (id == DELETE_MY_FAVORITE_BUTTON) {
						dbHelper.deleteProduct(Integer.toString(myFavoriteProductsList.get(Utility.fav_position_var).getId()));
						myFavoriteProductsList = dbHelper.getProductList();
						myFavAdapter.notifyDataSetChanged();
						if (myFavoriteProductsList.size() == 0) {
							noFavouritesadded = (TextView) findViewById(R.id.noFavAdded_deleteTVID);
							noFavouritesadded.setVisibility(View.VISIBLE);
						}
					} else if (id == LOGOUT) {
						try {
							if (dbHelper != null) {
								dbHelper.deleteLoginDetails();
								closeAllActivities();
							}
						} catch (Exception e) {
							if (e != null) {
								Log.w("-->Debug", e);
							}
						}
						Intent logoutIntent = new Intent(FavoritesListActivity.this, LoginActivity.class);
						logoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
						logoutIntent.putExtra("finish", true);
						startActivity(logoutIntent);
						LoginActivity.etUserId.setText("");
						LoginActivity.etPasswd.setText("");
						LoginActivity.subDomainURL_et.setText("");
						FavoritesListActivity.this.finish();
					} else if (id == MY_FAVOURITES) {
						alertDialog.dismiss();
					}
					alertDialog.dismiss();
				}
			});
			noBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					alertDialog.dismiss();
				}
			});
		} catch (Exception e) {
			if (null != e) {
				Log.w("-->", e);
			}
		}
	
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View rowView, int pos, long arg3) {
		super.onItemClick(arg0, rowView, pos, arg3);
		if (arg0.getId() == R.id.resultsListViewID) {
			if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				if (menuListView.getVisibility() == ListView.GONE) {
					Intent detailsIntent = new Intent(FavoritesListActivity.this, ProductDetailsActivity.class);
					detailsIntent.putExtra(ApplicationConstants.PRODUCT_ID_KEY,	myFavoriteProductsList.get(pos).getId());
					detailsIntent.putExtra(ApplicationConstants.COLOR_CODE_KEY, pos % 4);
					detailsIntent.putExtra(ApplicationConstants.PRODUCT_NAME_KEY, myFavoriteProductsList.get(pos).getName());
					detailsIntent.putExtra(ApplicationConstants.PRODUCT_HIGHLIGHT_KEY,	myFavoriteProductsList.get(pos).getHighlight());
					startActivity(detailsIntent);
				}
			} else {
				// The Custom Toast Layout Imported here -->by 
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_no_netowrk,
				(ViewGroup) findViewById(R.id.custom_toast_layout_id));
								 
				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent i = new Intent(FavoritesListActivity.this, SearchListActivity.class);
			i.putExtra("finish", true);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
			startActivity(i);
			headerTitle = null;
			FavoritesListActivity.this.finish();
		}
		return super.onKeyDown(keyCode, event);
	}
}
