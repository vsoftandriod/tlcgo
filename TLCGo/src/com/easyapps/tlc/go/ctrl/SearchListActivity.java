package com.easyapps.tlc.go.ctrl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.easyapps.tlc.go.ctrl.imagecache.SmartImageView;
import com.easyapps.tlc.go.ctrl.model.Category;
import com.easyapps.tlc.go.ctrl.model.NoticeId;
import com.easyapps.tlc.go.ctrl.model.User;
import com.easyapps.tlc.go.ctrl.service.TlcGoService;
import com.easyapps.tlc.go.ctrl.service.ServiceListener;
import com.easyapps.tlc.go.ctrl.utils.ApplicationConstants;
import com.easyapps.tlc.go.ctrl.utils.DatabaseHelper;
import com.easyapps.tlc.go.ctrl.utils.Utility;

/*This activity contains the search list with list of different categories.this is the first screen after the user
 *  login.Here user can suppose to select the category,type the location name and also keyword of the product.
 * When user clicks the search results,then that will show the all products(in ResultsListActivity) related to
 * the category,keyword and location.
 * */

@SuppressLint({ "DefaultLocale", "ShowToast" })
public class SearchListActivity extends HomeBasedActivity implements
		OnItemClickListener, OnClickListener, ServiceListener,
		OnLongClickListener  { //IImageURLTaskListener
	SearchListAdapter mAdapter;
	ArrayList<Category> categoryList;
	ArrayList<Category> categoryListTemp;
	List<NoticeId> noticeid;
	View loading;
	Button searchBtn;
	ListView searchListView;
	Button resetBtn;
	EditText locationET;
	EditText keywordET;
	private static final int SEARCH_LIST_SELECTION = 100022;
	// this is for login activity variables
	final private static int NO_NETWORK_CON = 100033;
	SmartImageView bannerIV;
	Bundle bundle = null;
	String loginIntent = null;
	boolean finishState;
	boolean doubleBackToExitPressedOnce = false;
	public static String headerTitle = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_list_main);
		registerBaseActivityReceiver();
		setDimensions();
		dbHelper = new DatabaseHelper(this);

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		dbHelper = new DatabaseHelper(this);
		headerTitle = getResources().getString(R.string.search_text);
		setHeaderTitle(headerTitle);

		categoryList = new ArrayList<Category>();
		categoryListTemp = new ArrayList<Category>();
		
		loading = (View) findViewById(R.id.loading);
		Button helpBtn = (Button) findViewById(R.id.helpBtnID);
		helpBtn.setTypeface(Utility.font_bold);
		helpBtn.getLayoutParams().width = (int) (Utility.screenWidth / 5.6);
		helpBtn.getLayoutParams().height = (int) (Utility.screenHeight / 16.0);
		helpBtn.setOnClickListener(this);
		locationET = (EditText) findViewById(R.id.enterLocationETID);

		locationET.setTypeface(Utility.font_reg);

		locationET.getLayoutParams().height = (int) (Utility.screenHeight / 16.0);
		keywordET = (EditText) findViewById(R.id.enterKeywordETID);

		keywordET.setTypeface(Utility.font_reg);

		keywordET.getLayoutParams().height = (int) (Utility.screenHeight / 16.0);
		searchBtn = (Button) findViewById(R.id.searchBtnID);
		searchBtn.setTypeface(Utility.font_bold);
		searchBtn.getLayoutParams().height = (int) (Utility.screenHeight / 16.0);
		searchBtn.setOnClickListener(this);
		resetBtn = (Button) findViewById(R.id.resetBtnID);
		resetBtn.setTypeface(Utility.font_bold);
		resetBtn.getLayoutParams().width = (int) (Utility.screenWidth / 5.6);
		resetBtn.getLayoutParams().height = (int) (Utility.screenHeight / 16.0);
		resetBtn.setOnClickListener(this);
		searchListView = (ListView) findViewById(R.id.searchListViewID);
		searchListView.setOnItemClickListener(this);

		locationET.setOnLongClickListener(this);
		keywordET.setOnLongClickListener(this);

		menuListView = (ListView) findViewById(R.id.menuListViewID);
		initialiseViews();

		bannerIV = (SmartImageView) findViewById(R.id.bannerIVID);
		bannerIV.getLayoutParams().height = (int) (Utility.screenHeight / 11.0);
		bannerIV.getLayoutParams().width = Utility.screenWidth;
		
		if (getIntent() != null) {
			try {
				bundle = getIntent().getExtras();
				if (bundle != null) {
					loginIntent = bundle.getString("LOGIN_ACTIVITY");
					// ---- > finish() all activities 
					finishState = bundle.getBoolean("finish", false);
				}
				if (loginIntent != null || bundle == null) {
					if (Utility.isOnline((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE))) {
						if (Utility.user != null) {
							TlcGoService.getTlcGoService().sendNoticeBoardCountIdRequset(this);
							TlcGoService.getTlcGoService().sendCategoriesListRequest(this);
						}
					} else {
						alertTitleStr = getResources().getString(R.string.alert_title_no_network_ava);
						alertMsgStr = getResources().getString(R.string.no_network_avail);
						alertYesStr = getResources().getString(R.string.ok_one);
						alertNoStr = null;
						showDialog(NO_NETWORK_CON);
					}
				} else if (finishState) {
					Intent i = new Intent(this, SearchListActivity.class);//here code stopped
					i.putExtra("LOGIN_ACTIVITY", "LOGIN_ACTIVITY");
					startActivity(i);
					SearchListActivity.this.finish();
					return;
				}
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
					Log.w("-->DEBUG", e);
				}
			}
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		try {
			System.gc();
			Runtime.getRuntime().gc();
			unRegisterBaseActivityReceiver();
		} catch (Exception e) {
			if (e != null) {
				Log.w("-->", e);
			}
		}
	}

	public class SearchListAdapter extends BaseAdapter {

		public SearchListAdapter(SearchListActivity searchListActivity) {

		}

		@Override
		public int getCount() {
			return categoryList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(int pos, View view, ViewGroup arg2) {
			View searchListRow = null;
			try {
				if (searchListRow == null) {
					inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					searchListRow = (ViewGroup) inflater.inflate(
							R.layout.search_list_item, null, false);
				}
				LinearLayout searchlistLL = (LinearLayout) searchListRow
						.findViewById(R.id.searchListsLLID);
				searchlistLL.getLayoutParams().height = (int) (Utility.screenHeight / 14.0);
				TextView category = (TextView) searchListRow
						.findViewById(R.id.categoryTVID);
				ImageView catImage = (ImageView) searchListRow
						.findViewById(R.id.catImageID);

				category.setText(categoryList.get(pos).getName());
				category.setTypeface(Utility.font_bold);
				category.setTextColor(getResources().getColor(R.color.handlife));
				
				String cat=categoryList.get(pos).getName();
				if(cat.toLowerCase().contains("automotive"))
				{
					catImage.setBackgroundResource(R.drawable.pin_type1);
				}
				
				else if(cat.toLowerCase().contains("dining"))
				{
					catImage.setBackgroundResource(R.drawable.pin_type2);
				}
				else if(cat.toLowerCase().contains("golf courses"))
				{
					catImage.setBackgroundResource(R.drawable.pin_type8);
				}
				else if(cat.toLowerCase().contains("golf handicaps") || cat.toLowerCase().contains("handicaps"))
				{
					catImage.setBackgroundResource(R.drawable.pin_type8);
				}
				else if(cat.toLowerCase().contains("golf insurance") || cat.toLowerCase().contains("insurance"))
				{
					catImage.setBackgroundResource(R.drawable.pin_type9);
				}
				else if(cat.toLowerCase().contains("health"))
				{
					catImage.setBackgroundResource(R.drawable.pin_type3);
				}
				else if(cat.toLowerCase().contains("home"))
				{
					catImage.setBackgroundResource(R.drawable.pin_type4);
				}
				else if(cat.toLowerCase().contains("lifestyle"))
				{
					catImage.setBackgroundResource(R.drawable.pin_type4);
				}
				else if(cat.toLowerCase().contains("entertainment") || cat.toLowerCase().contains("leisure"))
				{
					catImage.setBackgroundResource(R.drawable.pin_type5);
				}
				else if(cat.toLowerCase().contains("shop"))
				{
					catImage.setBackgroundResource(R.drawable.pin_type6);
				}
				else if(cat.toLowerCase().contains("travel"))
				{
					catImage.setBackgroundResource(R.drawable.pin_type7);
				}
				else 
				{
					catImage.setBackgroundResource(R.drawable.pin_type10);
				}

				RadioButton selectedRowRB = (RadioButton) searchListRow.findViewById(R.id.categoryRBID);
				if (categoryList.get(pos).isSelected())
					selectedRowRB.setChecked(true);
				else
					selectedRowRB.setChecked(false);
			
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
					Log.w("-->DEBUG", e);
				}
				return null;
			}
			
			return searchListRow;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View rowView, int pos,
			long arg3) {
		super.onItemClick(arg0, rowView, pos, arg3);
		if (arg0.getId() == R.id.searchListViewID) {
			try {
				if (menuListView.getVisibility() == ListView.GONE) {
					for (int i = 0; i < categoryList.size(); i++) {
						categoryList.get(i).setSelected(false);
					}
					categoryList.get(pos).setSelected(true);
					mAdapter.notifyDataSetChanged();
				}
			} catch (Exception e) {
				Log.w("-->DEUBUG", e);
			}
		}
	}

	@Override
	public void onClick(View view) {
		super.onClick(view);
			switch (view.getId()) {
			case R.id.helpBtnID:
				try {
					LayoutInflater inflater = getLayoutInflater();
					View helpDialogView = (View) inflater.inflate(R.layout.help_dialog,	null, false);
					final Dialog messageDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
					TextView windowTitle = (TextView) helpDialogView.findViewById(R.id.info_heading);
					windowTitle.setTypeface(Utility.font_reg);

					TextView helpDesc1TVID = (TextView) helpDialogView.findViewById(R.id.helpDesc1TVID);
					helpDesc1TVID.setTypeface(Utility.font_reg);

					TextView helpDesc2TVID = (TextView) helpDialogView.findViewById(R.id.helpDesc2TVID);
					helpDesc2TVID.setTypeface(Utility.font_reg);

					TextView helpDesc8TVID = (TextView) helpDialogView.findViewById(R.id.helpDesc8TVID);
					helpDesc8TVID.setTypeface(Utility.font_reg);

					TextView helpDesc3TVID = (TextView) helpDialogView.findViewById(R.id.helpDesc3TVID);
					helpDesc3TVID.setTypeface(Utility.font_reg);

					TextView helpDesc4TVID = (TextView) helpDialogView.findViewById(R.id.helpDesc4TVID);
					helpDesc4TVID.setTypeface(Utility.font_reg);

					TextView helpDesc5TVID = (TextView) helpDialogView.findViewById(R.id.helpDesc5TVID);
					helpDesc5TVID.setTypeface(Utility.font_reg);

					TextView helpDesc6TVID = (TextView) helpDialogView.findViewById(R.id.helpDesc6TVID);
					helpDesc6TVID.setTypeface(Utility.font_reg);

					TextView helpDesc7TVID = (TextView) helpDialogView.findViewById(R.id.helpDesc7TVID);
					helpDesc7TVID.setTypeface(Utility.font_reg);

					TextView helpDesc9TVID = (TextView) helpDialogView.findViewById(R.id.helpDesc9TVID);
					
					helpDesc9TVID.setText(getResources().getString(R.string.info_text9)+"\n \n"+getResources().getString(R.string.info_text10)+"\n \n"+getResources().getString(R.string.info_text11));
					
					helpDesc9TVID.setTypeface(Utility.font_reg);

					Window window = messageDialog.getWindow();
					window.setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND, WindowManager.LayoutParams.FLAG_DIM_BEHIND);
					window.setLayout((int) (8 * Utility.screenWidth / 9.2),	5 * Utility.screenHeight / 6);
					window.getAttributes().dimAmount = 0.7f;
					messageDialog.setCancelable(true);
					messageDialog.setContentView(helpDialogView);
					messageDialog.show();
					ImageView myhee = (ImageView) helpDialogView.findViewById(R.id.infoButtonIVID);
					// ImageView myhee=(ImageView)findViewById(R.id.infoButtonIVID);
					myhee.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							messageDialog.cancel();
						}
					});
				} catch (Exception e) {
					if (e != null) {
						Log.w("--> DEBUG", e);
					}
				}
				
				break;
			case R.id.searchBtnID:
				String location = locationET.getText().toString();
				String keyword = keywordET.getText().toString();
				String catID = null;
				for (int i = 0; i < categoryList.size(); i++) {
					if (categoryList.get(i).isSelected()) {
						catID = Integer.toString(categoryList.get(i).getCatID());
						break;
					}
				}
				if (catID == null) {
					alertTitleStr = getResources().getString(R.string.alert_title_search_field);
					alertMsgStr = getResources().getString(R.string.search_no);
					alertYesStr = getResources().getString(R.string.ok_one);
					alertNoStr = null;
					showDialog(SEARCH_LIST_SELECTION);
				} else {
					try {
						if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
							Intent resultIntent = new Intent(SearchListActivity.this, ResultsListActivity.class);
							resultIntent.putExtra(ApplicationConstants.CAT_ID_KEY, catID);
							resultIntent.putExtra(ApplicationConstants.LOCATION_KEY, location.trim());
							resultIntent.putExtra(ApplicationConstants.KEYWORK_KEY, keyword.trim());
							startActivity(resultIntent);
							//SearchListActivity.this.finish();
						} else {
							// The Custom Toast Layout Imported here
							LayoutInflater inflater = getLayoutInflater();
							View layout = inflater.inflate(R.layout.toast_no_netowrk,
							(ViewGroup) findViewById(R.id.custom_toast_layout_id));
										 
							// The actual toast generated here.
							Toast toast = new Toast(getApplicationContext());
							toast.setDuration(Toast.LENGTH_LONG);
							toast.setView(layout);
							toast.show();
						}
					} catch (Exception e) {
						if (e != null) {
							Log.w("--DEBUG", e);
						}
					}
				}
				break;
			case R.id.resetBtnID:
				try {
					keywordET.setText("");
					locationET.setText("");
					for (int i = 0; i < categoryList.size(); i++) {
						categoryList.get(i).setSelected(false);
					}
					mAdapter.notifyDataSetChanged();
				} catch (Exception e) {
					if (e != null) {
						Log.w("--DEBUG", e);
					}
				}
				break;
			}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onServiceComplete(Object response, int eventType) {
		try{
		if (eventType == 16) {
			if (response != null) {
				noticeid = (ArrayList<NoticeId>) response;
				int i = -1;
				if (noticeid != null)
				{
					List<String> noticesCountList=new ArrayList<String>();
					for(int n=0;n<noticeid.size();n++)
					{
						if(!(noticeid.get(n).getNoticeDetails()==null))
							noticesCountList.add(noticeid.get(n).getNoticeDetails());
					}
						if(noticesCountList.size()!=dbHelper.getExistingIDs().size())
						{
							for(int m=0;m<dbHelper.getExistingIDs().size();m++)
							{
								if(noticesCountList.contains(dbHelper.getExistingNoticeRealIDs().get(m)))
								{
									
								}
								else
								{
									dbHelper.deleteNoticeIdDetails(dbHelper.getExistingNoticeRealIDs().get(m));
								}
							}
						}
					
					for (int k = 0; k < noticeid.size(); k++) {
						if (noticeid.get(k).getNoticeDetails() != null) {
							i++;
							List<String> list = dbHelper.getExistingIDs();
							if (!list.contains(noticeid.get(k).getNoticeDetails())) {
								dbHelper.addnoticeiddetails(noticeid.get(k)
										.getNoticeDetails());
							}
						}
					}
					Log.v("---->", "Count = "+i);				
				}
			}
		}

		else if (eventType == 2 && response instanceof User) {
			if (response != null) {
				if (response instanceof String) {
				} else {
					dbHelper.addLoginDetails(Utility.user_Name,
							Utility.user_Password, Utility.user_website,
							dbHelper.getLoginDetails().getLoginDate());
					Utility.user = (User) response;
					// Utility.user=Translate.execute("How are you",
					// Language.ENGLISH, Language.FRENCH);
					categoryList = new ArrayList<Category>();
					categoryListTemp = new ArrayList<Category>();
					if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
						if (Utility.user != null) {
							TlcGoService.getTlcGoService().sendNoticeBoardCountIdRequset(this);
							TlcGoService.getTlcGoService().sendCategoriesListRequest(this);
						}
					} else {
						// The Custom Toast Layout Imported here
						LayoutInflater inflater = getLayoutInflater();
						View layout = inflater.inflate(R.layout.toast_no_netowrk,
						(ViewGroup) findViewById(R.id.custom_toast_layout_id));
									 
						// The actual toast generated here.
						Toast toast = new Toast(getApplicationContext());
						toast.setDuration(Toast.LENGTH_LONG);
						toast.setView(layout);
						toast.show();
					}
				}
			}
		}

		else {
			if(eventType!=16)
			{
			if (response != null) {
				if (response instanceof String) {
					loading.setVisibility(View.GONE);
				} else {
					if(eventType==4)
					{
						if (Utility.user != null) {
							if (Utility.user.getClientBanner() == null) {
								String bannerURL = ApplicationConstants.CLIENT_BANNER_WRAPPER+ Utility.user.getClient_id();
								newImagesLoading(bannerURL);
							} else {
								//onImageLoadComplete(Utility.user.getClientBanner());
							}
						}
					categoryList.clear();
					categoryList.addAll((Collection<Category>) response);
					if (categoryListTemp != null) {
						int position = categoryListTemp.size();
						categoryListTemp.addAll(categoryList);
						categoryList = categoryListTemp;
						searchListView.setSelection(position);
					}
					mAdapter = new SearchListAdapter(this);
					searchListView.setAdapter(mAdapter);
					loading.setVisibility(View.GONE);

					menuListView.setVisibility(ListView.VISIBLE);
					if(getIntent()!=null)
					{
						if(getIntent().getStringExtra("LOGIN_ACTIVITY")!=null)
						{
							applyMenuListSlideAnimation(-1, 0);
						}
						else
						{
							applyMenuListSlideAnimation(0, -1);
							menuListView.setVisibility(ListView.GONE);
						}
					}
					else
					{
						applyMenuListSlideAnimation(-1, 0);
					}
					
					try {
						//this is for notifying the user regarding the updates of notice boards
						if(dbHelper.getNoticeUpdateState().equals("NO_VALUE"))
						{
						dbHelper.setNoticeUpdateState("ON");
						Utility.setNotificationReceiver(this);
						}
					} catch (Exception e) {
						if (e != null) {
							e.printStackTrace();
							Log.w("-->DEBUG", e);
						}
					}
				}
			}
			}
		}
		}
		}catch(Exception e)
		{
			if (e != null) {
				e.printStackTrace();
				Log.w("-->DEBUG", e);
			}		
		}
	}
	
	private void newImagesLoading(String _bannerPath) {
		// Loader image - will be shown before loading image
	
		// Image url
        String image_url = _bannerPath;
        Log.w("-->", _bannerPath);
        try {
        	bannerIV.setImageUrl(image_url);
		} catch (OutOfMemoryError e) {
			if (e != null) {
				Log.w("-->DEBUG", e);
			}
		}
	}

	@Override
	protected void onRestart() {
		menuListView.setVisibility(ListView.GONE);
		super.onRestart();
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			AlertDialog alertDialog = null;
			LayoutInflater liYes = LayoutInflater.from(this);
			View callAddressView = liYes.inflate(R.layout.alert_all_in_one_dialog, null);
			AlertDialog.Builder adbrok = new AlertDialog.Builder(this);
			adbrok.setCancelable(false);
			adbrok.setView(callAddressView);
			alertDialog = adbrok.create();
			alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
			alertDialog.show();
			return alertDialog;
		} catch (Exception e) {
			if (e != null) {
				Log.w("-->", e);
			}
			return null;
		}
	}

	@Override
	protected void onPrepareDialog(final int id, Dialog dialog) {
		try {
			final AlertDialog Alert = (AlertDialog) dialog;
			alertTitleTV = (TextView) Alert.findViewById(R.id.alertYNTitleTVID);
			alertTitleTV.setTypeface(Utility.font_bold);
			alertTitleTV.setText(""+alertTitleStr);
			
			alertMessageTV = (TextView) Alert.findViewById(R.id.alertYNMessageTVID);
			alertMessageTV.setTypeface(Utility.font_reg);
			alertMessageTV.setText(""+alertMsgStr);
			
			yesBtn = (Button) Alert.findViewById(R.id.alertYESBtnID);
			yesBtn.setTypeface(Utility.font_bold);
			yesBtn.setText(""+alertYesStr);
			
			noBtn = (Button) Alert.findViewById(R.id.alertNOBtnID);
			if (alertNoStr == null) {
				noBtn.setVisibility(View.GONE);
			} else {
				noBtn.setTypeface(Utility.font_bold);
				noBtn.setText(""+alertNoStr);
			}
			Alert.setCancelable(false);
			yesBtn.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					switch (id) {
					case LOGOUT:
						try {
							if (dbHelper != null) {
								dbHelper.deleteLoginDetails();
								closeAllActivities();
							}
						} catch (Exception e) {
							if (e != null) {
								Log.w("-->Debug", e);
							}
						}
						Intent logoutIntent = new Intent(SearchListActivity.this, LoginActivity.class);
						logoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
						logoutIntent.putExtra("finish", true);
						startActivity(logoutIntent);
						LoginActivity.etUserId.setText("");
						LoginActivity.etPasswd.setText("");
						LoginActivity.subDomainURL_et.setText("");
						SearchListActivity.this.finish();
						Log.v(" Alert ID", "LOGOUT ID"+LOGOUT);
						break;
					case MY_FAVOURITES:
						Alert.dismiss();
						Log.v(" Alert ID", "MY_FAVOURITES ID"+MY_FAVOURITES);
						break;
					case SEARCH_LIST_SELECTION:
						Alert.dismiss();
						Log.v(" Alert ID", "ID"+SEARCH_LIST_SELECTION);
						break;
					case NO_NETWORK_CON:
						try {
							loading.setVisibility(View.GONE);
							if (menuListView.getVisibility() == View.GONE) {
								menuListView.setVisibility(View.VISIBLE);
								applyMenuListSlideAnimation(-1, 0);
							} else {
								applyMenuListSlideAnimation(0, -1);
								menuListView.setVisibility(View.GONE);
							}
						} catch (Exception e) {
							if (e != null) {
								Log.w("-->DEBUG", e);
							}
						}
						Log.v(" Alert ID", "ID"+SEARCH_LIST_SELECTION);
						break;
					default:
						break;
					}
					Alert.dismiss();
				}
			});
			noBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Alert.dismiss();
				}
			});
		} catch (Exception e) {
			if (null != e) {
				Log.w("-->", e);
			}
		}
	}

	private void setDimensions() {
		try {
			Display display = getWindowManager().getDefaultDisplay();
			int screenWidth = display.getWidth();
			int screenHeight = display.getHeight();
			Utility.screenWidth = screenWidth;
			Utility.screenHeight = screenHeight;
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("-->DEBUG", e);
			}
		}
	}

	@Override
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
	     if (keyCode==KeyEvent.KEYCODE_BACK) {
			if (doubleBackToExitPressedOnce) {
					closeAllActivities();
					SearchListActivity.this.finish();
					return doubleBackToExitPressedOnce;
				}
				doubleBackToExitPressedOnce = true;
				if (menuListView.getVisibility() == View.VISIBLE) {
					applyMenuListSlideAnimation(0, -1);
					menuListView.setVisibility(View.GONE);
				}
				Toast.makeText(SearchListActivity.this, "Press again to minimize the app.", 100).show();
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						doubleBackToExitPressedOnce = false;
					}
				}, 2000);
				return true;
	  }
	     return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public boolean onLongClick(View v) {
		boolean returnValue = false;
		try {
			EditText ed = (EditText) v;
			int stringLength = ed.getText().length();
			returnValue = Utility.copyPasteMethod(v, stringLength);

			return returnValue;
		
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("-->DEBUG", e);
			}
		}
		return returnValue;
	}
}
