package com.easyapps.tlc.go.ctrl;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.easyapps.tlc.go.ctrl.imagecache.SmartImageView;
import com.easyapps.tlc.go.ctrl.model.Product;
import com.easyapps.tlc.go.ctrl.service.TlcGoService;
import com.easyapps.tlc.go.ctrl.service.ServiceListener;
import com.easyapps.tlc.go.ctrl.utils.ApplicationConstants;
import com.easyapps.tlc.go.ctrl.utils.DatabaseHelper;
import com.easyapps.tlc.go.ctrl.utils.Utility;

public class ResultsListActivity extends HomeBasedActivity implements
		OnItemClickListener, ServiceListener, 
		OnScrollListener { //IImageURLTaskListener
	List<Product> productsList;
	List<Product> productsListTemp;
	ResultAdapter mAdapter;
	LayoutInflater inflater;
	View mLoading;
	ListView resultsListView;
	String catID = null;
	String location = null;
	String keyword = null;
	private int mvisibleItemCount = -1;
	private String fetchDirectionUP = Utility.FETCH_DIRECTION_UP;
	private String fetchDirection = "";
	private int visibleThreshold = 0;
	private int previousTotal = 0;
	private boolean loading = true;
	private int mfirstVisibleItem;;
	int productsCount = 0;
	private static final int NO_RESULT_FOUND = 10011;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.results_list);
		Utility.DailyDealsScreen = "null";
		Utility.FavouritesScreen ="null";
		Utility.hotOffersScreen = "null";
		Utility.AroundMeScreen = "null";
		Utility.ticketoffersScreen="null";
		
		try {
			registerBaseActivityReceiver();
			setHeaderTitle(getResources().getString(R.string.search_products_text));

			RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
			headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

			showBackButton();
			menuBtn = (Button) findViewById(R.id.menuBtnID);
			menuBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
			menuBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
			menuListView = (ListView) findViewById(R.id.menuListViewID);
			initialiseViews();
			dbHelper = new DatabaseHelper(this);
			mLoading = (View) findViewById(R.id.loading);
			resultsListView = (ListView) findViewById(R.id.resultsListViewID);
			productsList = new ArrayList<Product>();
			productsListTemp = new ArrayList<Product>();
			resultsListView.setOnItemClickListener(this);
			resultsListView.setOnScrollListener(this);
			if (getIntent() != null) {
				try {
					Bundle bundle = getIntent().getExtras();
					if (bundle != null) {
						catID = bundle.getString(ApplicationConstants.CAT_ID_KEY);
						location = bundle.getString(ApplicationConstants.LOCATION_KEY);
						keyword = bundle.getString(ApplicationConstants.KEYWORK_KEY);
					}
					if (Utility
							.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
						if (catID != null) {
							TlcGoService.getTlcGoService().sendSearchProductsRequest(this, catID, location, keyword, 0, 30);
						} else {
							TlcGoService.getTlcGoService().sendHotOffersRequest(this);
						}
					} else {
						// The Custom Toast Layout Imported here
						LayoutInflater inflater = getLayoutInflater();
						View layout = inflater.inflate(R.layout.toast_no_netowrk,
								(ViewGroup) findViewById(R.id.custom_toast_layout_id));

						// The actual toast generated here.
						Toast toast = new Toast(getApplicationContext());
						toast.setDuration(Toast.LENGTH_LONG);
						toast.setView(layout);
						toast.show();
						ResultsListActivity.this.finish();
					}
				} catch (Exception e) {
					if (e != null) {
						e.printStackTrace();
						Log.w("HARI-->DEBUG", e);
					}
				}
			}
		} catch (NullPointerException e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
	}

	@Override
	public void onClick(View v) {
		try {
			if (v.getId() == R.id.backToSearchBtnID) {
				ResultsListActivity.this.finish();
			}
			super.onClick(v);
		} catch (NullPointerException e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
	}

	public class ResultAdapter extends BaseAdapter {

		public ResultAdapter(ResultsListActivity resultsListActivity) {

		}

		@Override
		public int getCount() {
			return productsList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(int pos, View view, ViewGroup arg2) {
			View resultsListRow = null;
			try {
				if (resultsListRow == null) {
					inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					resultsListRow = (View) inflater.inflate(
							R.layout.results_list_item, null, false);
				}
				LinearLayout rowLL2 = (LinearLayout) resultsListRow
						.findViewById(R.id.resultListItemLLID);
				rowLL2.getLayoutParams().height = (int) (Utility.screenHeight / 8.5);
				LinearLayout rowLL = (LinearLayout) resultsListRow
						.findViewById(R.id.resultItemLLID);
				TextView productNameTV = (TextView) resultsListRow
						.findViewById(R.id.productTVID);
				productNameTV.setTypeface(Utility.font_bold);

				TextView highlightTV = (TextView) resultsListRow
						.findViewById(R.id.offerTVID);
				highlightTV.setTypeface(Utility.font_reg);
				productNameTV.setText(productsList.get(pos).getName());
				highlightTV.setText(productsList.get(pos).getHighlight());
				switch (pos % 4) {
				case 0:
					rowLL.setBackgroundResource(R.color.result_color_one);
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
					break;
				case 1:
					rowLL.setBackgroundResource(R.color.result_color_two);
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
					break;
				case 2:
					rowLL.setBackgroundResource(R.color.result_color_three);
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
					break;
				case 3:
					rowLL.setBackgroundResource(R.color.result_color_four);
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
					break;
				}
			
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
					Log.w("HARI-->DEBUG", e);
					return null;
				}
			}
			return resultsListRow;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View rowView, int pos,
			long arg3) {
		super.onItemClick(arg0, rowView, pos, arg3);
		if (arg0.getId() == R.id.resultsListViewID) {
			try {
				if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					if (menuListView.getVisibility() == ListView.GONE) {
						Intent detailsIntent = new Intent(ResultsListActivity.this,	ProductDetailsActivity.class);
						detailsIntent.putExtra(ApplicationConstants.PRODUCT_ID_KEY,
								productsList.get(pos).getId());
						detailsIntent.putExtra(ApplicationConstants.COLOR_CODE_KEY,
								pos % 4);
						detailsIntent.putExtra(
								ApplicationConstants.PRODUCT_NAME_KEY, productsList
										.get(pos).getName());
						detailsIntent.putExtra(
								ApplicationConstants.PRODUCT_HIGHLIGHT_KEY,
								productsList.get(pos).getHighlight());
						detailsIntent.putExtra(Utility.FINISHED_STATUS, "RESULT_LIST_SCREEN");
						startActivity(detailsIntent);
					}
			}else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater.inflate(R.layout.toast_no_netowrk,
							(ViewGroup) findViewById(R.id.custom_toast_layout_id));

					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_LONG);
					toast.setView(layout);
					toast.show();
				}
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
					Log.w("HARI-->DEBUG", e);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onServiceComplete(Object response, int eventType) {
		try {
			if (eventType != 16) {
				if (response != null) {
					if (response instanceof String) {
						mLoading.setVisibility(View.GONE);
						alertTitleStr = getResources().getString(R.string.alert_title_nosearchitem_insearch);
						alertMsgStr = getResources().getString(R.string.no_results);
						alertYesStr = getResources().getString(R.string.ok_one);
						alertNoStr = null;
						showDialog(NO_RESULT_FOUND);
					} else {
						if(eventType==5 || eventType==6)
						{
						productsList = (ArrayList<Product>) response;

						productsCount = productsCount + productsList.size();

						if (!(productsCount > 0)) {
							mLoading.setVisibility(View.GONE);
							alertTitleStr = getResources().getString(R.string.alert_title_nosearchitem_insearch);
							alertMsgStr = getResources().getString(R.string.no_results);
							alertYesStr = getResources().getString(R.string.ok_one);
							alertNoStr = null;
							showDialog(NO_RESULT_FOUND);

							if (Utility.user.getClientBanner() == null) {
								//ImageURLTask imgTask = new ImageURLTask();
								//imgTask.setListener(this);
								//imgTask.execute(ApplicationConstants.CLIENT_BANNER_WRAPPER+ Utility.user.getClient_id());
								String bannerURL = ApplicationConstants.CLIENT_BANNER_WRAPPER+ Utility.user.getClient_id();
								newImagesLoading(bannerURL);
							} else {
								//onImageLoadComplete(Utility.user.getClientBanner());
							}
							mLoading.setVisibility(View.GONE);
						} else {
							if (Utility.user.getClientBanner() == null) {
								//ImageURLTask imgTask = new ImageURLTask();
								//imgTask.setListener(this);
								//imgTask.execute(ApplicationConstants.CLIENT_BANNER_WRAPPER+ Utility.user.getClient_id());
								String bannerURL = ApplicationConstants.CLIENT_BANNER_WRAPPER+ Utility.user.getClient_id();
								newImagesLoading(bannerURL);
							} else {
								//onImageLoadComplete(Utility.user.getClientBanner());
							}

							mLoading.setVisibility(View.GONE);

							if (fetchDirection
									.equalsIgnoreCase(fetchDirectionUP)) {

								int position = productsListTemp.size();
								productsListTemp.addAll(productsList);
								productsList = productsListTemp;

								mAdapter = new ResultAdapter(this);
								resultsListView.setAdapter(mAdapter);
								resultsListView.setScrollingCacheEnabled(false);
								if (productsList.size() > 0
										&& mvisibleItemCount != -1)
									resultsListView.setSelection(position
											- mvisibleItemCount + 2);
								else
									resultsListView.setSelection(position);
							}

							if (mAdapter == null) {
								mAdapter = new ResultAdapter(this);
								resultsListView.setAdapter(mAdapter);
							} else {
								mAdapter.notifyDataSetChanged();
							}
						}
					}
				}
				}
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	private void newImagesLoading(String bannerURL) {
		// Loader image - will be shown before loading image
		SmartImageView bannerIV = (SmartImageView) findViewById(R.id.bannerIVID);
		bannerIV.getLayoutParams().height = (int) (Utility.screenHeight / 11.0);
		//bannerIV.getLayoutParams().width = Utility.screenWidth * 1;
		
		//int loader = R.drawable.loading_imp;
		//int loader = bannerIV.getWidth();
		//Log.w("Hari-->", String.valueOf(loader));
		//int loader = bannerIV.getLayoutParams().width = Utility.screenWidth;
        
		// Image url
        String image_url = bannerURL;
        Log.w("Hari-->", bannerURL);
        
        // ImageLoader class instance
      //  BannerImageLoader imgLoader = new BannerImageLoader(getApplicationContext());
        
        // whenever you want to load an image from url
        // call DisplayImage function
        // url - image url to load
        // loader - loader image, will be displayed before getting image
        // image - ImageView 
        //imgLoader.DisplayImage(image_url, loader, bannerIV);
        try {
        	bannerIV.setImageUrl(image_url);
       // 	imgLoader.DisplayImage(image_url, bannerIV);
		} catch (OutOfMemoryError e) {
			if ( e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		try {
			mvisibleItemCount = visibleItemCount;
			if (firstVisibleItem > mfirstVisibleItem && !loading
					&& (firstVisibleItem + visibleItemCount == totalItemCount)
					&& totalItemCount > 0 && totalItemCount != visibleItemCount) {
				fetchDirection = fetchDirectionUP;
				System.out.println("firstVisibleItem" + firstVisibleItem);
				System.out.println("visibleItemCount" + visibleItemCount);
				System.out.println("totalItemCount" + totalItemCount);
				if (!loading
						&& (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)
						&& totalItemCount != 0
						&& mLoading.getVisibility() != View.VISIBLE) {
					sendRequestWithScrollDirection(fetchDirectionUP, totalItemCount);
					loading = true;
				}
			}

			if (loading) {
				if (totalItemCount > previousTotal) {
					loading = false;
					previousTotal = totalItemCount;

				}
			}
			mfirstVisibleItem = firstVisibleItem;
		
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	private void sendRequestWithScrollDirection(String DirectiontoFetch,
			int totalItemCount) {
		try {
			if (productsList.size() == totalItemCount) {
				mLoading.setVisibility(View.VISIBLE);
				fetchDirection = DirectiontoFetch;
				productsListTemp = productsList;

				if ((productsCount - 30) >= 0) {
					if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
						TlcGoService.getTlcGoService().sendSearchProductsRequest(this, catID, location,	keyword, productsCount, 10);
					} else {
						// The Custom Toast Layout Imported here
						LayoutInflater inflater = getLayoutInflater();
						View layout = inflater.inflate(R.layout.toast_no_netowrk, (ViewGroup) findViewById(R.id.custom_toast_layout_id));
						
						// The actual toast generated here.
						Toast toast = new Toast(getApplicationContext());
						toast.setDuration(Toast.LENGTH_LONG);
						toast.setView(layout);
						toast.show();
						ResultsListActivity.this.finish();
					}
				} else {
					mLoading.setVisibility(View.GONE);
				}
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			AlertDialog alertDialog = null;
			LayoutInflater liYes = LayoutInflater.from(this);
			View callAddressView = liYes.inflate(R.layout.alert_all_in_one_dialog, null);
			AlertDialog.Builder adbrok = new AlertDialog.Builder(this);
			adbrok.setCancelable(false);
			adbrok.setView(callAddressView);
			alertDialog = adbrok.create();
			alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
			alertDialog.show();
			return alertDialog;
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->", e);
			}
			return null;
		}
	}

	@Override
	protected void onPrepareDialog(final int id, Dialog dialog) {
		try {
			final AlertDialog hariAlert = (AlertDialog) dialog;
			alertTitleTV = (TextView) hariAlert.findViewById(R.id.alertYNTitleTVID);
			alertTitleTV.setTypeface(Utility.font_bold);
			alertTitleTV.setText(""+alertTitleStr);
			
			alertMessageTV = (TextView) hariAlert.findViewById(R.id.alertYNMessageTVID);
			alertMessageTV.setTypeface(Utility.font_reg);
			alertMessageTV.setText(""+alertMsgStr);
			
			yesBtn = (Button) hariAlert.findViewById(R.id.alertYESBtnID);
			yesBtn.setTypeface(Utility.font_bold);
			yesBtn.setText(""+alertYesStr);
			
			noBtn = (Button) hariAlert.findViewById(R.id.alertNOBtnID);
			if (alertNoStr == null) {
				noBtn.setVisibility(View.GONE);
			} else {
				noBtn.setTypeface(Utility.font_bold);
				noBtn.setText(""+alertNoStr);
			}
			hariAlert.setCancelable(false);
			yesBtn.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					switch (id) {
					case LOGOUT:
						try {
							if (dbHelper != null) {
								dbHelper.deleteLoginDetails();
								closeAllActivities();
							}
						} catch (Exception e) {
							if (e != null) {
								Log.w("Hari-->Debug", e);
							}
						}
						Intent logoutIntent = new Intent(ResultsListActivity.this, LoginActivity.class);
						logoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
						logoutIntent.putExtra("finish", true);
						startActivity(logoutIntent);
						LoginActivity.etUserId.setText("");
						LoginActivity.etPasswd.setText("");
						LoginActivity.subDomainURL_et.setText("");
						ResultsListActivity.this.finish();
						Log.v("HARI Alert ID", "LOGOUT ID"+LOGOUT);
						break;
					case MY_FAVOURITES:
						Log.v("HARI Alert ID", "MY_FAVOURITES ID"+MY_FAVOURITES);
						break;
					case NO_RESULT_FOUND:
						ResultsListActivity.this.finish();
						Log.v("HARI Alert ID", "NO_RESULT_FOUND ID"+NO_RESULT_FOUND);
						break;
					default:
						break;
					}
					hariAlert.dismiss();
				}
			});
			noBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					hariAlert.dismiss();
				}
			});
		} catch (Exception e) {
			if (null != e) {
				Log.w("HARI-->", e);
			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			ResultsListActivity.this.finish();
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	protected void onPause() {
        super.onPause();
        try {
            System.gc();
            Runtime.getRuntime().gc();
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->", e);
			}
		}
     }

	@Override
	protected void onDestroy() {
		super.onDestroy();
		try {
			unRegisterBaseActivityReceiver();
			Runtime.getRuntime().gc();
			System.gc();
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->", e);
			}
		}
	}	
	
	@Override
	public void onLowMemory() {
		super.onLowMemory();
		System.gc();
	}
}
