package com.easyapps.tlc.go.ctrl;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

 /*
   This is Alarm Receiver class
   
  */

public class Alarm extends BroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent intent) {
		try {
			Intent in=new Intent(context, AlertDialogActivity.class);
			in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			String text=context.getResources().getString(R.string.alarm_recvd);
			Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
			context.startActivity(in);
		} catch (NullPointerException _e) {
			if (_e != null) {
				Log.w("-->DEBUG", _e);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("-->DEBUG", e);
			}
		}
	}
}
