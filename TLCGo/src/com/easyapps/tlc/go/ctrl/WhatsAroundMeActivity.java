package com.easyapps.tlc.go.ctrl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.NYXDigital.NiceSupportMapFragment;
import com.easyapps.tlc.go.ctrl.imagecache.SmartImageView;
import com.easyapps.tlc.go.ctrl.model.Product;
import com.easyapps.tlc.go.ctrl.service.TlcGoService;
import com.easyapps.tlc.go.ctrl.service.ServiceListener;
import com.easyapps.tlc.go.ctrl.utils.ApplicationConstants;
import com.easyapps.tlc.go.ctrl.utils.Utility;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 *  This Activity class will show the list of near by product in MapView.
 *  When user clicks on map pin info he will be switched to the product details 
 *  view.Here we used android maps V2. 
 * 
 */
@SuppressWarnings("unused")
public class WhatsAroundMeActivity extends HomeBasedActivity implements
		OnClickListener, ServiceListener, LocationListener,
		OnItemClickListener, OnMarkerClickListener,
		OnInfoWindowClickListener { //IImageURLTaskListener
	private View loading;
	private GoogleMap googleMap;
	private ArrayList<Product> product;
	private Marker markerforCL, marker1, marker2, marker3, marker4, marker5, marker6, marker7;
	private Location location;
	public static boolean noticeboardcount = false;
	public static String headerTitle = null;
	private int i;
	private int productsLength = 0;
	private boolean tempStatus = false;

	List<String> tempList;
	GPSTracker mGPS = null;

	//Initialize to a non-valid zoom value
	private float previousZoomLevel = -1.0f;
	private boolean isZooming = false;
	private static final int MAP_SETTINGS = 10044;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		registerBaseActivityReceiver();
		setContentView(R.layout.whats_around_me);
		
		Utility.hotOffersScreen ="null";
		Utility.FavouritesScreen ="null";
		Utility.DailyDealsScreen ="null";
		Utility.ResultScreen ="null";
		
		headerTitle = getResources().getString(R.string.whats_around_me_text);
		setHeaderTitle(headerTitle);

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		menuBtn = (Button) findViewById(R.id.menuBtnID);
		menuBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		menuBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		menuListView = (ListView) findViewById(R.id.menuListViewID);
		menuBtn.setOnClickListener(this);
		initialiseViews();
		loading = (View) findViewById(R.id.loading);
		
		tempList = new ArrayList<String>();
		try {
			if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				if (Utility.user != null) {
					if (Utility.user.getClientBanner() == null) {
						String bannerURL = ApplicationConstants.CLIENT_BANNER_WRAPPER+ Utility.user.getClient_id();
						newImagesLoading(bannerURL);
						initilizeMaps();
					} 
				}
			} else {
				// The Custom Toast Layout Imported here
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_no_netowrk,(ViewGroup) findViewById(R.id.custom_toast_layout_id));

				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
				WhatsAroundMeActivity.this.finish();
				headerTitle = null;
			}
		} catch (Exception e) {
			Log.w("--->", e);
		}
	}
	
	private void initilizeMaps() {
		if (googleMap == null) {
			NiceSupportMapFragment mapFragment = (NiceSupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapfragmentId);
			googleMap = mapFragment.getMap();
			mapFragment.setPreventParentScrolling(false);
		}

		// check if map is created successfully or not
		if (googleMap == null) {
			Toast.makeText(getApplicationContext(), "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
		} else {
			googleMap.setMyLocationEnabled(true);
			googleMap.getUiSettings().setMyLocationButtonEnabled(true);
			if (mGPS == null) {
				mGPS = new GPSTracker(WhatsAroundMeActivity.this);
			}

			// check if mGPS object is created or not
			if (mGPS != null && location == null) {
				location = mGPS.getLocation();
			}

			// check if location is created or not
			if (location != null) {
				onLocationChanged(location);
				googleMap.getUiSettings().setMyLocationButtonEnabled(true);
				TlcGoService.getTlcGoService().sendNearestLatLonRequest(WhatsAroundMeActivity.this, location.getLatitude(), location.getLongitude());
			} else {
				loading.setVisibility(View.GONE);
				alertTitleStr = getResources().getString(R.string.map_settings_title);
       		  	alertMsgStr = getResources().getString(R.string.map_settings_msg);
       		  	alertYesStr = getResources().getString(R.string.menu_settings);
       		  	alertNoStr = getResources().getString(R.string.cancel_one);
				showDialog(MAP_SETTINGS);
			}
		}
	}

	private void newImagesLoading(String _bannerPath) {
		// Loader image - will be shown before loading image
		SmartImageView banner = (SmartImageView) findViewById(R.id.bannerIVID);
		banner.getLayoutParams().height = (int) (Utility.screenHeight / 11.0);
		banner.getLayoutParams().width = Utility.screenWidth;
		
		// Image url
        String image_url = _bannerPath;
        Log.w("-->", _bannerPath);
        
        try {
        	banner.setImageUrl(image_url);
        	// imgLoader.DisplayImage(image_url, banner);
		} catch (OutOfMemoryError e) {
			if (e != null) {
				Log.w("-->DEBUG", e);
			}
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		if (location != null) {
			LatLng currentlocation = new LatLng(location.getLatitude(),
					location.getLongitude());
			markerforCL = googleMap.addMarker(new MarkerOptions()
					.position(currentlocation).title("I am Here:")
					.icon(BitmapDescriptorFactory.fromResource(R.drawable.me)));
			googleMap.setOnMarkerClickListener(this);
			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(currentlocation, 12);
			googleMap.moveCamera(cameraUpdate);
			googleMap.animateCamera(cameraUpdate);
			
		//	googleMap.animateCamera(CameraUpdateFactory.zoomTo(12));
			googleMap.setOnCameraChangeListener(getCameraChangeListener());
		}
	}
	public OnCameraChangeListener getCameraChangeListener()
	{
	    return new OnCameraChangeListener() 
	    {
	        @Override
	        public void onCameraChange(CameraPosition position) 
	        {
	            Log.d("Zoom", "Zoom: " + position.zoom);

	            if(previousZoomLevel != position.zoom)
	            {
	                isZooming = true;
	            }

	            previousZoomLevel = position.zoom;
	        }
	    };
	}
	@Override
	public void onProviderDisabled(String provider) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onServiceComplete(Object response, int eventType) {

		if (response != null) {
			if (response instanceof String) {
			} else {
				if (eventType != 16) {
					try {
						if(eventType==8)
						{
						product = new ArrayList<Product>();
						product = (ArrayList<Product>) response;

						showAllPins();
						}
					}

					catch (Exception e) {
						if (e != null) {
							Log.w("-->DEBUG", e);
						}
					}
				}
			}
		}
		loading.setVisibility(View.GONE);
	}

	@SuppressLint("ShowToast")
	private void showAllPins() {
		Double nlat = 0.0, nlong = 0.0;
		final Double COORDINATE_OFFSET = 0.00002;
		productsLength = product.size();

		if (productsLength > 300) {
			productsLength = (productsLength / 2);
		}
		DecimalFormat dtime = new DecimalFormat("#.#####");
		for (i = 0; i < productsLength; i++) {
			if (i == 0) {
				Toast.makeText(this, "Loading please wait.....", 5000).show();
			}

			if (i == 0) {
				nlat = Double.valueOf(dtime.format(Double.parseDouble(product.get(i).getLatitude())));
				nlong = Double.valueOf(dtime.format(Double.parseDouble(product.get(i).getLongitude())));
			} else {
				nlat = Double.valueOf(dtime.format(Double.parseDouble(product.get(i).getLatitude())));
				nlong = Double.valueOf(dtime.format(Double.parseDouble(product.get(i).getLongitude())));
				for (int k = 0; k < tempList.size(); k++) {
					if (tempList.get(k).equals(nlat + "," + nlong)) {

						if (!(i > 30)) {
							nlat = nlat + COORDINATE_OFFSET
									+ (((0.001) * (i / 25.0)) / 10.0);
							nlong = nlong + COORDINATE_OFFSET
									+ (((0.001) * (i / 25.0)) / 10.0);
						} else {
							nlat = nlat + COORDINATE_OFFSET
									+ (((0.001) * ((i % 29) / 25.0)) / 10.0);
							nlong = nlong + COORDINATE_OFFSET
									+ (((0.001) * ((i % 29) / 25.0)) / 10.0);
						}
						tempStatus = true;
						break;
					}
			}
				if (tempStatus == false) {
					nlat = Double.valueOf(dtime.format(Double.parseDouble(product.get(i).getLatitude())));
					nlong = Double.valueOf(dtime.format(Double.parseDouble(product.get(i).getLongitude())));
				}
			}

			if (Integer.parseInt(product.get(i).getPinType()) == 1) {
				marker1 = googleMap.addMarker(new MarkerOptions()
				.position(new LatLng(nlat, nlong))
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.pin_type1))
						.title(product.get(i).getName()));
				marker1.showInfoWindow();
				googleMap.setOnMarkerClickListener(this);
				googleMap.setOnInfoWindowClickListener(this);

			} else if (Integer.parseInt(product.get(i).getPinType()) == 2) {
				marker2 = googleMap.addMarker(new MarkerOptions()
						.position(new LatLng(nlat, nlong))
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.pin_type2))
						.title(product.get(i).getName()));
				marker2.showInfoWindow();
				googleMap.setOnMarkerClickListener(this);
				googleMap.setOnInfoWindowClickListener(this);
			} else if (Integer.parseInt(product.get(i).getPinType()) == 3) {
				marker3 = googleMap.addMarker(new MarkerOptions()
						.position(new LatLng(nlat, nlong))
						.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_type3))
						.title(product.get(i).getName()));
				marker3.showInfoWindow();
				googleMap.setOnMarkerClickListener(this);
				googleMap.setOnInfoWindowClickListener(this);

			} else if (Integer.parseInt(product.get(i).getPinType()) == 4) {
				marker4 = googleMap.addMarker(new MarkerOptions()
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.pin_type4))
						.position(new LatLng(nlat, nlong))
						.title(product.get(i).getName()));
				marker4.showInfoWindow();
				googleMap.setOnMarkerClickListener(this);
				googleMap.setOnInfoWindowClickListener(this);
			} else if (Integer.parseInt(product.get(i).getPinType()) == 5) {
				marker5 = googleMap.addMarker(new MarkerOptions()
						.position(new LatLng(nlat, nlong))
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.pin_type6))
						.title(product.get(i).getName()));
				marker5.showInfoWindow();
				googleMap.setOnMarkerClickListener(this);
				googleMap.setOnInfoWindowClickListener(this);

			} else if (Integer.parseInt(product.get(i).getPinType()) == 6) {
				marker6 = googleMap.addMarker(new MarkerOptions()
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.pin_type7))
						.position(new LatLng(nlat, nlong))
						.title(product.get(i).getName()));
				marker6.showInfoWindow();
				googleMap.setOnMarkerClickListener(this);
				googleMap.setOnInfoWindowClickListener(this);
			} else if (Integer.parseInt(product.get(i).getPinType()) == 7) {
				marker7 = googleMap.addMarker(new MarkerOptions()
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.pin_type5))
						.position(new LatLng(nlat, nlong))
						.title(product.get(i).getName()));
				marker7.showInfoWindow();
				googleMap.setOnMarkerClickListener(this);
				googleMap.setOnInfoWindowClickListener(this);
			}
			product.get(i).setLatitude(Double.toString(nlat));
			product.get(i).setLongitude(Double.toString(nlong));
			tempList.add(nlat + "," + nlong);
			tempStatus = false;
			Log.v("pins", nlat + "," + nlong);
		}
		loading.setVisibility(View.GONE);
	}

	@Override
	public boolean onMarkerClick(Marker marker) {
		return false;
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			AlertDialog alertDialog = null;
			LayoutInflater liYes = LayoutInflater.from(this);
			View callAddressView = liYes.inflate(R.layout.alert_all_in_one_dialog, null);
			AlertDialog.Builder adbrok = new AlertDialog.Builder(this);
			adbrok.setCancelable(false);
			adbrok.setView(callAddressView);
			alertDialog = adbrok.create();
			alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
			alertDialog.show();
			return alertDialog;
		} catch (Exception e) {
			if (e != null) {
				Log.w("-->", e);
			}
			return null;
		}
	}

	@Override
	protected void onPrepareDialog(final int id, Dialog dialog) {
		try {
			final AlertDialog Alert = (AlertDialog) dialog;
			alertTitleTV = (TextView) Alert.findViewById(R.id.alertYNTitleTVID);
			alertTitleTV.setTypeface(Utility.font_bold);
			alertTitleTV.setText(""+alertTitleStr);
			
			alertMessageTV = (TextView) Alert.findViewById(R.id.alertYNMessageTVID);
			alertMessageTV.setTypeface(Utility.font_reg);
			alertMessageTV.setText(""+alertMsgStr);
			
			yesBtn = (Button) Alert.findViewById(R.id.alertYESBtnID);
			yesBtn.setTypeface(Utility.font_bold);
			yesBtn.setText(""+alertYesStr);
			
			noBtn = (Button) Alert.findViewById(R.id.alertNOBtnID);
			if (alertNoStr == null) {
				noBtn.setVisibility(View.GONE);
			} else {
				noBtn.setTypeface(Utility.font_bold);
				noBtn.setText(""+alertNoStr);
			}
			Alert.setCancelable(false);
			yesBtn.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					switch (id) {
					case LOGOUT:
						try {
							if (dbHelper != null) {
								dbHelper.deleteLoginDetails();
								closeAllActivities();
							}
						} catch (Exception e) {
							if (e != null) {
								Log.w("-->Debug", e);
							}
						}
						Intent logoutIntent = new Intent(WhatsAroundMeActivity.this, LoginActivity.class);
						logoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
						logoutIntent.putExtra("finish", true);
						startActivity(logoutIntent);
						LoginActivity.etUserId.setText("");
						LoginActivity.etPasswd.setText("");
						LoginActivity.subDomainURL_et.setText("");
						WhatsAroundMeActivity.this.finish();
						Log.v(" Alert ID", "LOGOUT ID"+LOGOUT);
						break;
					case MY_FAVOURITES:
						Alert.dismiss();
						Log.v(" Alert ID", "MY_FAVOURITES ID"+MY_FAVOURITES);
						break;
					case MAP_SETTINGS:
						try {
							Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
							WhatsAroundMeActivity.this.startActivity(intent);
							WhatsAroundMeActivity.this.finish();
							headerTitle = null;
						} catch (Exception e) {
							Log.w("-->", e);
						}
						Log.v(" Alert ID", "MAP_SETTINGS ID"+MAP_SETTINGS);
						break;
					default:
						break;
					}
					Alert.dismiss();
				}
			});
			noBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Alert.dismiss();
				}
			});
		} catch (Exception e) {
			if (null != e) {
				Log.w("-->", e);
			}
		}
	}

	@Override
	public void onInfoWindowClick(Marker marker) {

		for (int j = 0; j < productsLength; j++) {
			Log.v("hai", "this is 1");
			DecimalFormat dtime = new DecimalFormat("#.#####");
			marker.setTitle(product.get(j).getName());

			Double val = marker.getPosition().latitude;
			val = Double.valueOf(dtime.format(val));
			System.out
					.println((Double.valueOf(dtime.format(Double
							.parseDouble((product.get(j).getLatitude())))))
							+ " "
							+ Double.valueOf(dtime.format(marker.getPosition().latitude)));
			if ((Double.valueOf(dtime.format(Double.parseDouble((product.get(j)
					.getLatitude()))))).equals(Double.valueOf(dtime
					.format(marker.getPosition().latitude)))) {
				Log.v("hai", "this is entered 1");

				System.out.println(marker.getPosition().latitude + "      "
						+ marker.getPosition().latitude);

				if (Utility
						.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					if (menuListView.getVisibility() == ListView.GONE) {
						Intent detailsIntent = new Intent(WhatsAroundMeActivity.this, ProductDetailsActivity.class);
						detailsIntent.putExtra(ApplicationConstants.PRODUCT_ID_KEY, product.get(j).getId());
						detailsIntent.putExtra(ApplicationConstants.PRODUCT_NAME_KEY, product.get(j).getName());
						detailsIntent.putExtra(Utility.FINISHED_STATUS, "AROUND_ME_SCREEN");
						startActivity(detailsIntent);
					}
				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater.inflate(R.layout.toast_no_netowrk, (ViewGroup) findViewById(R.id.custom_toast_layout_id));

					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_LONG);
					toast.setView(layout);
					toast.show();
				}
				break;
			}
		}

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		super.onKeyDown(keyCode, event);
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent i = new Intent(WhatsAroundMeActivity.this, SearchListActivity.class);
			i.putExtra("finish", true);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
			startActivity(i);
			WhatsAroundMeActivity.this.finish();
			headerTitle = null;
		}
		return true;
	}
		
	@Override
	public void onLowMemory() {
		super.onLowMemory();
		System.gc();
	}
}
