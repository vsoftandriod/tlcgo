package com.easyapps.tlc.go.ctrl.xml;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.easyapps.tlc.go.ctrl.model.Product;

public class HotOffersParser {
	List<Product> productsList;
	public void internalXMLParse(String response, List<Product> productsList) {
		try {
			this.productsList = productsList;
		String _node,_element;
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;
		
			db = factory.newDocumentBuilder();
		
        InputSource inStream = new InputSource();
        inStream.setCharacterStream(new StringReader(response));
        Document doc = db.parse(inStream);  
        doc.getDocumentElement().normalize();
        
        NodeList list2 =    doc.getElementsByTagName("product");
        _node = new String();
        _element = new String();      
        
        for (int i=0;i<list2.getLength();i++){    
        	NodeList childNodes = list2.item(i).getChildNodes();
        	Product product = new Product();
             for(int j=0;j<childNodes.getLength();j++){
            	 Node value=childNodes.item(j).getChildNodes().item(0);
            	 _node=childNodes.item(j).getNodeName();
                 if(value != null){
                	 _element=value.getNodeValue(); 
                	 updateField(_node,_element, product);
                 }
             }
             productsList.add(product);
        }
        } catch (ParserConfigurationException e) {
			e.printStackTrace();
        } catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private void updateField(String node, String element, Product product) {
		if(node.equals("id"))
        {
			product.setId(Integer.parseInt(element));
        }
		else if(node.equals("highlight"))
        {
			product.setHighlight(element);
        }
		else if(node.equals("name"))
        {
			product.setName(element);
        }
		else if(node.equals("latitude"))
        {
			product.setLatitude(element);
        }
		else if(node.equals("longitude"))
        {
			product.setLongitude(element);
        }
		else if(node.equals("pin_type"))
        {
			product.setPinType(element);
        }
	
	}

}
