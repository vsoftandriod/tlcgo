package com.easyapps.tlc.go.ctrl.xml;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.easyapps.tlc.go.ctrl.model.User;

public class UserDetailsParser {
	public void internalXMLParse(String response, User user) {
		try {
		String _node,_element;		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;		
			db = factory.newDocumentBuilder();		
        InputSource inStream = new InputSource();
        inStream.setCharacterStream(new StringReader(response));
        Document doc = db.parse(inStream);  
        doc.getDocumentElement().normalize();
        
        NodeList list =    doc.getElementsByTagName("*");
        _node = new String();
        _element = new String();   
        for (int i=0;i<list.getLength();i++){           
             Node value=list.item(i).		
              getChildNodes().item(0);
             _node=list.item(i).getNodeName();
             if(value != null){
            	 _element=value.getNodeValue(); 
            	 updateField(_node,_element, user);
             }
        }
        } catch (ParserConfigurationException e) {
			e.printStackTrace();
        } catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void updateField(String node, String element,User user){
		         
         if(node.equals("id"))
           {
        	 user.setId(Integer.parseInt(element));
           }

         else if(node.equals("client_id"))
           {
             user.setClient_id(Integer.parseInt(element));
             
           }
         else if(node.equals("domain_id"))
           {
        	  user.setDomain_id(Integer.parseInt(element));
             
           }
         else if(node.equals("type"))
           {
        	   user.setType(element);
           }
          
         else if(node.equals("username"))
           {
            user.setUsername(element);
           }
         else if(node.equals("email"))
           {
        	   user.setEmail(element);
           }
         else if(node.equals("first_name"))
           {
            user.setFirst_name(element);
           }
         else if(node.equals("last_name"))
           {
            user.setLast_name(element);
           }
         else  if(node.equals("state"))
           {
        	   user.setState(element);
            
           }
         else if(node.equals("country"))
         {
          user.setCountry(element);
         }
       else if(node.equals("mobile"))
         {
      	   user.setMobile(element);
         }
       else if(node.equals("card_ext"))
         {
          user.setCard_ext(element);
         }
       else if(node.equals("client_name"))
         {
          user.setClient_name(element);
         }
       else  if(node.equals("newsletter"))
         {
      	   user.setNewsletter(element);
          
         }
    }
}
