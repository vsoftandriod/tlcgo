package com.easyapps.tlc.go.ctrl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.easyapps.tlc.go.ctrl.imagecache.SmartImageView;
import com.easyapps.tlc.go.ctrl.utils.ApplicationConstants;
import com.easyapps.tlc.go.ctrl.utils.Utility;

/*
 * This class will contain the details about the noticeboards.When user clicks on
 * notices list item (in NoticeBoardActivity),he will be redirected to this details screen.
 * The details will be shown in WebView.
 * */

@SuppressLint("SetJavaScriptEnabled")
public class NoticeBoardDetailsActivity extends HomeBasedActivity {// implements
	// IImageURLTaskListener{
	View loading;
	String noticeId;
	String noticeSubject = null;
	String noticeDetails = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notice_board_details);
		registerBaseActivityReceiver();

		setHeaderTitle(getResources().getString(R.string.notice_board_text));
		showBackButton();
		menuBtn = (Button) findViewById(R.id.menuBtnID);
		menuBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		menuBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		menuListView = (ListView) findViewById(R.id.menuListViewID);
		initialiseViews();
		loading = (View) findViewById(R.id.loading);
		loading.setVisibility(View.GONE);

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);
		if (Utility
				.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			if (Utility.user != null) {
				if (Utility.user.getMyMembershipCard() == null) {
					String bannerURL = ApplicationConstants.CLIENT_BANNER_WRAPPER
							+ Utility.user.getClient_id();
					newImagesLoading(bannerURL);
				}
			}
		} else {
			// The Custom Toast Layout Imported here
			LayoutInflater inflater = getLayoutInflater();
			View layout = inflater.inflate(R.layout.toast_no_netowrk,
					(ViewGroup) findViewById(R.id.custom_toast_layout_id));

			// The actual toast generated here.
			Toast toast = new Toast(getApplicationContext());
			toast.setDuration(Toast.LENGTH_LONG);
			toast.setView(layout);
			toast.show();
		}
		if (getIntent() != null) {
			int colorResource = 0;
			Bundle bundle = getIntent().getExtras();
			if (bundle != null) {
				noticeId = bundle.getString(ApplicationConstants.NOTICE_ID_KEY);
				noticeSubject = bundle.getString(ApplicationConstants.NOTICE_NAME_KEY);
				noticeDetails = bundle.getString(ApplicationConstants.NOTICE_DETAILS_KEY);
				colorResource = bundle.getInt(ApplicationConstants.COLOR_CODE_KEY);
			}
			TextView noticeNameTV = (TextView) findViewById(R.id.productTVID);
			View resultListItem = (View) findViewById(R.id.resultListItemID);
			resultListItem.getLayoutParams().height = (int) (Utility.screenHeight / 10.5);
			switch (colorResource) {
			case 0:
				resultListItem.setBackgroundResource(R.color.result_color_one);
				noticeNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
				break;
			case 1:
				resultListItem.setBackgroundResource(R.color.result_color_two);
				noticeNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
				break;
			case 2:
				resultListItem.setBackgroundResource(R.color.result_color_three);
				noticeNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
				break;
			case 3:
				resultListItem.setBackgroundResource(R.color.result_color_four);
				noticeNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
				break;
			}

			noticeNameTV.setTypeface(Utility.font_bold);
			if (noticeSubject != null) {
				noticeNameTV.setText(noticeSubject);
			}

			loadInWebView(noticeDetails);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		try {
			System.gc();
			unRegisterBaseActivityReceiver();
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("-->DEBUG", e);
			}
		}
	}

	private void newImagesLoading(String _bannerPath) {
		// Loader image - will be shown before loading image
		SmartImageView bannerIV = (SmartImageView) findViewById(R.id.bannerIVID);
		bannerIV.getLayoutParams().height = (int) (Utility.screenHeight / 11.0);
	
		// Image url
		String image_url = _bannerPath;
		Log.w("-->", _bannerPath);

		try {
			bannerIV.setImageUrl(image_url);
		} catch (OutOfMemoryError e) {
			if (e != null) {
				Log.w("-->DEBUG", e);
			}
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.backToSearchBtnID) {
			startActivity(new Intent(NoticeBoardDetailsActivity.this,
					NoticeBoardActivity.class));
			NoticeBoardDetailsActivity.this.finish();
		}
		super.onClick(v);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(NoticeBoardDetailsActivity.this,
					NoticeBoardActivity.class));
			NoticeBoardDetailsActivity.this.finish();
		}
		return super.onKeyDown(keyCode, event);
	}

	private void loadInWebView(String webContent) {
		WebView webView = (WebView) findViewById(R.id.detailsTVID);

		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setAllowFileAccess(true);
		webView.getSettings().setLoadsImagesAutomatically(true);
		webView.getSettings().setUserAgentString(
				Locale.getDefault().getLanguage());
		WebSettings settings = webView.getSettings();
		settings.setDefaultTextEncodingName("utf-8");
		webView.setWebViewClient(new WebViewClient());
		webView.setWebViewClient(new WebViewClient() {

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				if (url.startsWith("tel:")) {
					Intent intent = new Intent(Intent.ACTION_DIAL, Uri
							.parse(url));
					startActivity(intent);
				} else if (url.startsWith("http:") || url.startsWith("https:")) {
					Intent intent = new Intent(Intent.ACTION_VIEW, Uri
							.parse(url));
					startActivity(intent);
				} else if (url.startsWith("mailto:")) {
					MailTo mt = MailTo.parse(url);
					Intent i = Utility.EmailIntent(NoticeBoardDetailsActivity.this,
							mt.getTo(), mt.getSubject(), mt.getBody(),
							mt.getCc());
					startActivity(i);
					view.reload();
					return true;
				} else {
					view.loadUrl(url);
				}
				return true;
			}
		});
		/*
		 * String summary = Html.fromHtml( product.getDetails() + "\n" + "\n" +
		 * product.getText()).toString();
		 */
		String summary = "<html><body style=\"font-family:Helvetica;line-height:20px\">"
				+ webContent + "</body></html>";

		summary = summary.replaceAll("//", "");
		// create text file
		if (!Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED))
			Log.d("GIN", "No SDCARD");
		else {
			File direct = new File(Environment.getExternalStorageDirectory()
					+ "/GIN");

			if (!direct.exists()) {
				if (direct.mkdir()) {
					// directory is created;
				}
			}

			try {
				File root = new File(Environment.getExternalStorageDirectory()
						+ "/GIN");
				if (root.canWrite()) {
					File file = new File(root, "GINnoticedetails.html");
					FileWriter fileWriter = new FileWriter(file);
					BufferedWriter out = new BufferedWriter(fileWriter);
					if (summary.contains("<iframe")) {
						try {
							int a = summary.indexOf("<iframe");
							int b = summary.indexOf("</iframe>");
							summary = summary.replace(
									summary.subSequence(a, b), "");
						} catch (Exception e) {
							if (e != null) {
								e.printStackTrace();

							}
						}
					}
					out.write(summary);
					out.close();
				}
			} catch (IOException e) {
				if (e != null) {
					e.printStackTrace();

				}
			}

			if (!Environment.getExternalStorageState().equals(
					Environment.MEDIA_MOUNTED)) {
				Log.d("GIN", "No SDCARD");
			} else {

				webView.loadUrl("file://"
						+ Environment.getExternalStorageDirectory() + "/GIN"
						+ "/GINnoticedetails.html");
				webView.setWebViewClient(new WebViewClient() {
					@Override
					public void onReceivedError(WebView view, int errorCode,
							String description, String failingUrl) {
						Log.i("WEB_VIEW_TEST", "error code:" + errorCode);

						super.onReceivedError(view, errorCode, description,
								failingUrl);
					}
				});

			}
		}
	}
}
