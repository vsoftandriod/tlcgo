package com.easyapps.tlc.go.ctrl.timer;


import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.easyapps.tlc.go.ctrl.R;

public class Settings extends PreferenceActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);
	}
}
