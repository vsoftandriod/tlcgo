package com.easyapps.tlc.go.ctrl.imagecache;

import android.content.Context;
import android.graphics.Bitmap;

public interface SmartImage {
    public Bitmap getBitmap(Context context);
}