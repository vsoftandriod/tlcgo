package com.easyapps.tlc.go.ctrl;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.easyapps.tlc.go.ctrl.imagecache.WebImageCache;
import com.easyapps.tlc.go.ctrl.model.LoginDetails;
import com.easyapps.tlc.go.ctrl.model.User;
import com.easyapps.tlc.go.ctrl.service.TlcGoService;
import com.easyapps.tlc.go.ctrl.service.ServiceListener;
import com.easyapps.tlc.go.ctrl.utils.AnimeUtils;
import com.easyapps.tlc.go.ctrl.utils.ApplicationConstants;
import com.easyapps.tlc.go.ctrl.utils.DatabaseHelper;
import com.easyapps.tlc.go.ctrl.utils.Utility;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseInstallation;
import com.parse.PushService;

/*this is login screen*/

@SuppressLint({ "SimpleDateFormat", "ShowToast", "ClickableViewAccessibility", "InflateParams" })
public class LoginActivity extends Activity implements OnClickListener,
		ServiceListener, AnimationListener, OnLongClickListener {
	public static EditText etUserId;
	public static EditText etPasswd;
	RelativeLayout parentRL;
	public static EditText subDomainURL_et;
	RelativeLayout parentSubLayout;
	ArrayAdapter<String> adapter;
	Button btnLogin, firstTimeLoginBtn;
	private InputMethodManager imm;
	DatabaseHelper dbHelper;
	ImageView splashIV;
	public static EditText msNoet;
	public static EditText webAdds;
	// public static AlertDialog dialogDetails2;
	SimpleDateFormat formatter;
	Dialog dialog;

	public Animation movement5;
	final private static int FIRST_TIME_LOGIN_BUTTON = 1;
	final private static int NO_NETWORK_CON = 2;
	final private static int LOGIN_FAILED = 3;
	final private static int BOTH_EMPTY = 4;

	// Membership validation
	public TextView tv12;
	LoginDetails loginDetails;
	RelativeLayout loadingPanel;
	Bundle bundle = null;
	boolean finishState;
	
	private TextView alertTitleTV, alertMessageTV;
	private Button yesBtn, noBtn;
	private String alertTitleStr, alertMsgStr, alertYesStr, alertNoStr;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity);

		Utility.deleteCmpleteCacheData(LoginActivity.this);
		
		// Push Notifications Adding...................
		// Parse.initialize(this, YOUR_APPLICATION_ID, YOUR_CLIENT_KEY);
		Parse.initialize(this, "FTPUGwmnhqznLxDlHWBv8yaDLVVc6Ld06UgW838R", "kW0vewyIGL3BMRsPfiKqqyNIAXNfPgtxLLDCFLD2");

		PushService.setDefaultPushCallback(this, AppPushNotificationActivity.class);
		ParseInstallation.getCurrentInstallation().saveInBackground();
		ParseAnalytics.trackAppOpened(getIntent());

		setDimensions();
		ApplicationConstants.USERNAME_PASSWORD_FIELDS = getResources()
				.getString(R.string.uname_paswd_app);
		ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL = getResources()
				.getString(R.string.invalid_url_estb);
		ApplicationConstants.UNABLETOESTABLISHCONNECTION = getResources()
				.getString(R.string.unable_to_estb);
		dbHelper = new DatabaseHelper(this);
		etUserId = (EditText) findViewById(R.id.usernameETID);

		etUserId.setTypeface(Utility.font_reg);
		
		parentSubLayout = (RelativeLayout) findViewById(R.id.loginRLID);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		params.setMargins(Utility.screenWidth / 32, Utility.screenWidth / 32, Utility.screenWidth / 32, Utility.screenWidth / 32);
		params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		parentSubLayout.setLayoutParams(params);

		etUserId.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		etPasswd = (EditText) findViewById(R.id.passwordETID);
		etPasswd.setTypeface(Utility.font_reg);

		etPasswd.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		subDomainURL_et = (EditText) findViewById(R.id.subDomainETID);
		subDomainURL_et.setTypeface(Utility.font_reg);

		subDomainURL_et.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		btnLogin = (Button) findViewById(R.id.loginBtnID);
		btnLogin.setTypeface(Utility.font_bold);
		btnLogin.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		btnLogin.setOnClickListener(this);

		
		/*etUserId.setText("sasi");
		etPasswd.setText("sasi123");
		subDomainURL_et.setText("www.myrewards.com.au");*/
		
		
		etUserId.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				getWindow().setSoftInputMode(
						WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
				return false;
			}
		});
		
		etPasswd.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				getWindow().setSoftInputMode(
						WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
				return false;
			}
		});
		
		
		
		subDomainURL_et.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				getWindow().setSoftInputMode(
						WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
				return false;
			}
		});
		
		etUserId.setOnLongClickListener(this);
		etPasswd.setOnLongClickListener(this);
		subDomainURL_et.setOnLongClickListener(this);
		
		GPSTracker mGPS = new GPSTracker(this);
		if (mGPS.canGetLocation) {
			double mLat = mGPS.getLatitude();
			double mLong = mGPS.getLongitude();
			Utility.mLat = mLat;
			Utility.mLng = mLong;

			Log.v("mLatmLatmLat", "=" + mLat);
			Log.v("mLongmLongmLongmLong", "=" + mLong);

		} else {
			// can't get the location
		}
		splashIV = (ImageView) findViewById(R.id.splashIVID);
		loadingPanel = (RelativeLayout) findViewById(R.id.loadingPanelNew);
		splashIV.setVisibility(ImageView.GONE);

		try {
			loginDetails = dbHelper.getLoginDetails();
		} catch (Exception e) {
			if (e != null) {
				Log.w("-->DEBUG", e);
			}
		}
		Date loginDate = null;
		Date currentDate = null;
		// Date Format: MMM DD, YYYY hh:mm:ss ex: Mar 11, 2013 10:03:08 PM
		if (loginDetails != null) {
			try {
				formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				try {
					loginDate = formatter.parse(loginDetails.getLoginDate());
				} catch (Exception e) {
					if (e != null) {
						e.printStackTrace();
						Log.w("-->DEBUG", e);
					}
				}
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Calendar cal = Calendar.getInstance();
				String currentDateString = dateFormat.format(cal.getTime());
				try {
					currentDate = formatter.parse(currentDateString);
				} catch (ParseException e) {
					if (e != null) {
						e.printStackTrace();
						Log.w("-->DEBUG", e);
					}
				}

				long diff = currentDate.getTime() - loginDate.getTime();
				long days = diff / (1000 * 60 * 60 * 24);
				if (days < 7) {
					splashIV.setVisibility(ImageView.GONE);
					if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
						TlcGoService.getTlcGoService().sendLoginRequest(this, loginDetails.getUsername(),loginDetails.getPassword(),loginDetails.getSubDomain());
					} else {
						alertTitleStr = getResources().getString(R.string.alert_title_no_network_ava);
						alertMsgStr = getResources().getString(R.string.no_network_avail);
						alertYesStr = getResources().getString(R.string.ok_one);
						alertNoStr = null;
						showDialog(NO_NETWORK_CON);
					}
				} else {
					try {
					     dbHelper.deleteLoginDetails();
					} catch (Exception e) {
						if (e != null) {
							e.printStackTrace();
							Log.v("-->", e.getMessage());
					   }
					}
					loginDetails=null;
					splashIV.setVisibility(ImageView.GONE);
					loadingPanel.setVisibility(View.GONE);
					// Animation //
					parentSubLayout.setVisibility(View.VISIBLE);
					// movement5 =
					
					movement5 = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.accelerate_login_page);
					movement5.reset();
					movement5.setFillAfter(true);
					movement5.setAnimationListener(this);
					parentSubLayout.startAnimation(movement5);
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("-->DEBUG", e);
				}
			}
		} else {
			try {
				splashIV.setVisibility(ImageView.GONE);
				loadingPanel.setVisibility(View.GONE);

				parentSubLayout.setVisibility(View.VISIBLE);
				// movement5 = AnimationUtils.loadAnimation(this,R.anim.animation_test5_forlogin);
				movement5 = AnimationUtils.loadAnimation(this, R.anim.accelerate_login_page);
				movement5.reset();
				movement5.setFillAfter(true);
				movement5.setAnimationListener(this);
				parentSubLayout.startAnimation(movement5);
			} catch (Exception e) {
				if (e != null) {
					Log.w("-->DEBUG", e);
				}
			}
		}

		firstTimeLoginBtn = (Button) findViewById(R.id.firstTimeloginBtnID);
		firstTimeLoginBtn.setTypeface(Utility.font_bold);
		firstTimeLoginBtn.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		firstTimeLoginBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				showDialog(FIRST_TIME_LOGIN_BUTTON);
			}
		});

		
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		try {
			// start/stop animation
			AnimeUtils.startViewAnimation(findViewById(R.id.imageView1), hasFocus);
			AnimeUtils.startViewAnimation(findViewById(R.id.loginRLID),	hasFocus);
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("-->DEBUG", e);
			}
		}
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			
				AlertDialog alertDialog = null;
				LayoutInflater liYes = LayoutInflater.from(this);
				View callAddressView = liYes.inflate(R.layout.alert_all_in_one_dialog, null);
				AlertDialog.Builder adbrok = new AlertDialog.Builder(this);
				adbrok.setCancelable(false);
				adbrok.setView(callAddressView);
				alertDialog = adbrok.create();
				alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
				alertDialog.show();
				return alertDialog;
			
		} catch (Exception e) {
			if (null != e) {
				Log.w("-->", e);
			}
			return null;
		}
	}

	@Override
	protected void onPrepareDialog(final int id, Dialog dialog1) {
		dialog = dialog1;
		
			final AlertDialog Alert = (AlertDialog) dialog;
			alertTitleTV = (TextView) Alert.findViewById(R.id.alertYNTitleTVID);
			alertTitleTV.setTypeface(Utility.font_bold);
			alertTitleTV.setText(""+alertTitleStr);
			
			alertMessageTV = (TextView) Alert.findViewById(R.id.alertYNMessageTVID);
			alertMessageTV.setTypeface(Utility.font_reg);
			alertMessageTV.setText(""+alertMsgStr);
			
			yesBtn = (Button) Alert.findViewById(R.id.alertYESBtnID);
			yesBtn.setTypeface(Utility.font_bold);
			yesBtn.setText(""+alertYesStr);
			
			noBtn = (Button) Alert.findViewById(R.id.alertNOBtnID);
			if (alertNoStr == null) {
				noBtn.setVisibility(View.GONE);
			} else {
				noBtn.setTypeface(Utility.font_bold);
				noBtn.setText(""+alertNoStr);
			}
			
			Alert.setCancelable(false);
			
			yesBtn.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if (id == NO_NETWORK_CON) {
						loadingPanel.setVisibility(View.GONE);
					} else if (id == LOGIN_FAILED) {
						if (parentSubLayout.getVisibility() == View.GONE) {
							parentSubLayout.setVisibility(View.VISIBLE);
							loadingPanel.setVisibility(View.GONE);
							try {
								if (dbHelper != null) {
									dbHelper.deleteLoginDetails();
									loginDetails = dbHelper.getLoginDetails();
								}
							} catch (Exception e) {
								if (e != null) {
									e.printStackTrace();
									Log.w("-->DEBUG", e);
								}
							}
						}
					
					} 
					Alert.dismiss();					
				}
			});
			
			noBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Alert.dismiss();
				}
			});
		
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.loginBtnID:
			try {
				String userId = etUserId.getText().toString();
				String passwd = etPasswd.getText().toString();
				String subDomainURL =subDomainURL_et.getText().toString();
			//	String subDomainURL = ApplicationConstants.subDomainURLString;
				
				if ((userId != null && userId.trim().length() > 0) && (passwd != null && passwd.trim().length() > 0)) {
					if (subDomainURL.trim().length() > 0) {
						try {
							issueRequest(userId, passwd, subDomainURL);
						} catch (Exception e) {
							if (e != null) {
								Log.w("-->DEBUG", e);
							}
						}
					} else {
						alertTitleStr = getResources().getString(R.string.warg_alert);
						alertMsgStr = getResources().getString(R.string.all_fields_error);
						alertYesStr = getResources().getString(R.string.ok_one);
						alertNoStr = null;
						showDialog(BOTH_EMPTY);
						btnLogin.setEnabled(true);
					}
				} else {
					alertTitleStr = getResources().getString(R.string.warg_alert);
					alertMsgStr = getResources().getString(R.string.all_fields_error);
					alertYesStr = getResources().getString(R.string.ok_one);
					alertNoStr = null;
					showDialog(BOTH_EMPTY);
					btnLogin.setEnabled(true);
				}
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
					Log.w("-->DEBUG", e);
				}
			}
			break;
		}
	}

	private void issueRequest(String userId, String password,
			String subDomainURL) {
		try {
			imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(etUserId.getWindowToken(), 0);
			imm.hideSoftInputFromWindow(etPasswd.getWindowToken(), 0);
			System.out.println("password: " + password);
			if (Utility.isOnline((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE))) {
				TlcGoService.getTlcGoService().sendLoginRequest(this, userId, password, subDomainURL);
				splashIV.setVisibility(ImageView.VISIBLE);
			} else {
				alertTitleStr = getResources().getString(R.string.alert_title_no_network_ava);
				alertMsgStr = getResources().getString(R.string.no_network_avail);
				alertYesStr = getResources().getString(R.string.ok_one);
				alertNoStr = null;
				showDialog(NO_NETWORK_CON);
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("-->DEBUG", e);
			}
		}
	}

	@Override
	public void onServiceComplete(Object response, int eventType) {
		try {
			if (eventType == 1 || eventType == 2) {
				if (response != null) {
					if (response instanceof String) {
						alertTitleStr = getResources().getString(R.string.alert_title_login_failed);
						alertMsgStr = getResources().getString(R.string.uname_paswd_app);
						alertYesStr = getResources().getString(R.string.ok_one);
						alertNoStr = null;
						showDialog(LOGIN_FAILED);
						splashIV.setVisibility(ImageView.GONE);
					} else {
						if (loginDetails == null) {
							try {
								DateFormat dateFormat = new SimpleDateFormat(
										"yyyy/MM/dd HH:mm:ss");
								Calendar cal = Calendar.getInstance();

								dbHelper.addLoginDetails(etUserId.getText()
										.toString(), etPasswd.getText()
										.toString(), subDomainURL_et.getText().toString(), dateFormat.format(cal
										.getTime()));
							} catch (Exception e) {
								if (e != null) {
									e.printStackTrace();
									Log.w("-->DEBUG", e);
								}
							}
							Utility.user_Name = etUserId.getText().toString();
							Utility.user_Password = etPasswd.getText().toString();
							Utility.user_website =subDomainURL_et.getText().toString();
							//Utility.user_website = ApplicationConstants.subDomainURLString;
						}
						if (Utility.user_Name == null) {
							Utility.user_Name = dbHelper.getLoginDetails().getUsername();
							Utility.user_Password = dbHelper.getLoginDetails().getPassword();
							Utility.user_website = dbHelper.getLoginDetails().getSubDomain();
						}
						Utility.user = (User) response;

						Intent i = new Intent(LoginActivity.this, SearchListActivity.class);
						i.putExtra("LOGIN_ACTIVITY", "LOGIN_ACTIVITY");
						startActivity(i);
						LoginActivity.this.finish();
					}
				}
			} 
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("-->DEBUG", e);
			}
		}
	}

	private void setDimensions() {
		try {
			Display display = getWindowManager().getDefaultDisplay();
			int screenWidth = display.getWidth();
			int screenHeight = display.getHeight();
			Utility.screenWidth = screenWidth;
			Utility.screenHeight = screenHeight;
			parentRL = (RelativeLayout) findViewById(R.id.loginParentRLID);

			Utility.font_bold = Typeface.createFromAsset(this.getAssets(),"helvetica_bold.ttf");
			Utility.font_reg = Typeface.createFromAsset(this.getAssets(),"helvetica_reg.ttf");
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("-->DEBUG", e);
			}
		}
	}

	@Override
	protected void onRestart() {
		splashIV.setVisibility(ImageView.GONE);
		super.onRestart();
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onLongClick(View v) {
		boolean returnValue;
		try {
			EditText ed = (EditText) v;
			int stringLength = ed.getText().length();
			returnValue = Utility.copyPasteMethod(v, stringLength);

		} catch (Exception e) {
			returnValue = false;
		}
		return returnValue;
	}
}
