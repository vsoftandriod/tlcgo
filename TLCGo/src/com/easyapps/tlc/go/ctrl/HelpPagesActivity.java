package com.easyapps.tlc.go.ctrl;

import java.util.List;
import java.util.Vector;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.easyapps.tlc.go.ctrl.imagecache.SmartImageView;
import com.easyapps.tlc.go.ctrl.model.HelpPageFive;
import com.easyapps.tlc.go.ctrl.model.HelpPageFour;
import com.easyapps.tlc.go.ctrl.model.HelpPageOne;
import com.easyapps.tlc.go.ctrl.model.HelpPageSix;
import com.easyapps.tlc.go.ctrl.model.HelpPageThree;
import com.easyapps.tlc.go.ctrl.model.HelpPageTwo;
import com.easyapps.tlc.go.ctrl.utils.ApplicationConstants;
import com.easyapps.tlc.go.ctrl.utils.Utility;

/** 
 * The <code>ViewPagerFragmentActivity</code> class is the fragment activity
 * hosting the ViewPager
 * 
 * We will show the Help Screen Pages here.
 */
public class HelpPagesActivity extends HomeBasedActivity implements
		OnClickListener, OnItemClickListener, AnimationListener {
	/** maintains the pager adapter */
	private PagerAdapter mPagerAdapter;
	private View loading;
	public static String headerTitle = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.helpviewpager_layout);
		registerBaseActivityReceiver();
		
		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		menuBtn = (Button) findViewById(R.id.menuBtnID);
		menuBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		menuBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		menuListView = (ListView) findViewById(R.id.menuListViewID);
		menuBtn.setOnClickListener(this);

		initialiseViews();
		headerTitle = getResources().getString(R.string.help_title_text);  
		setHeaderTitle(headerTitle);

		loading = (View) findViewById(R.id.loading);
		loading.setVisibility(View.GONE);
		
		try {
			if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				if (Utility.user != null) {
					if (Utility.user.getClientBanner() == null) {
						String bannerURL = ApplicationConstants.CLIENT_BANNER_WRAPPER+ Utility.user.getClient_id();
						try {
							newImagesLoading(bannerURL);
						} catch (Exception e) {
							if (e != null) {
								Log.w("-->DEBUG", e);
							}
						}
					} 
			} else {
				// The Custom Toast Layout Imported here
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_no_netowrk,
				(ViewGroup) findViewById(R.id.custom_toast_layout_id));
							 
				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
				HelpPagesActivity.this.finish();
				headerTitle = null;
				}
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("-->DEBUG", e);
			}
		}
		try
		{
		// initialsie the pager
		this.initialisePaging();
		}
		catch(Exception e)
		{
			if (e != null) {
				e.printStackTrace();
				Log.w("-->DEBUG", e);
			}
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		try {
			System.gc();
			Runtime.getRuntime().gc();
			unRegisterBaseActivityReceiver();
		} catch (Exception e) {
			if (e != null) {
				Log.w("-->DEBUG", e);
			}
		}
	}
	
	@Override
	protected void onPause() {
        super.onPause();
        try {
             System.gc();
             Runtime.getRuntime().gc();
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("-->DEBUG", e);
			}
		}
     }
	
	private void newImagesLoading(String _bannerPath) {
		// Loader image - will be shown before loading image
		SmartImageView banner = (SmartImageView) findViewById(R.id.bannerIVID);
		banner.getLayoutParams().height = (int) (Utility.screenHeight / 11.0);
		banner.getLayoutParams().width = Utility.screenWidth;

		// Image url
        String image_url = _bannerPath;
        Log.w("-->", _bannerPath);
        
        try {
        	banner.setImageUrl(image_url);
		} catch (OutOfMemoryError e) {
			if ( e != null) {
				Log.w("-->DEBUG", e);
			}
		}
	}

	/**
	 * Initialise the fragments to be paged
	 */
	private void initialisePaging() {
		try {
			List<Fragment> fragments = new Vector<Fragment>();
			fragments.add(Fragment.instantiate(this, HelpPageOne.class.getName()));
			fragments.add(Fragment.instantiate(this, HelpPageTwo.class.getName()));
			fragments.add(Fragment.instantiate(this, HelpPageThree.class.getName()));
			fragments.add(Fragment.instantiate(this, HelpPageFour.class.getName()));
			fragments.add(Fragment.instantiate(this, HelpPageFive.class.getName()));
			fragments.add(Fragment.instantiate(this, HelpPageSix.class.getName()));
			this.mPagerAdapter = new PagerAdapter(super.getSupportFragmentManager(), fragments);
			ViewPager pager = (ViewPager) super.findViewById(R.id.viewpager);
			pager.setAdapter(this.mPagerAdapter);
		} catch (Exception e) {
			if (e != null) {
				Log.w("--> DEBUG", e);
			}
		}
	}

	@Override
	public void onAnimationEnd(Animation animation) {
	}
	@Override
	public void onAnimationRepeat(Animation animation) {
	}
	@Override
	public void onAnimationStart(Animation animation) {
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			Intent i = new Intent(HelpPagesActivity.this, SearchListActivity.class);
			i.putExtra("finish", true);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
			startActivity(i);
			HelpPagesActivity.this.finish();
			headerTitle = null;
		}
		return super.onKeyDown(keyCode, event);
	}
}
