package com.easyapps.tlc.go.ctrl.model;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.easyapps.tlc.go.ctrl.R;
import com.easyapps.tlc.go.ctrl.utils.Utility;

public class HelpPageFive extends Fragment {
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v=(LinearLayout)inflater.inflate(R.layout.help_pages, container, false);
		TextView image = (TextView)v.findViewById(R.id.page1);
		image.setBackgroundResource(R.drawable.imag_help35);
		
		TextView textview1=(TextView)v.findViewById(R.id.page1_IVID);
		textview1.setMovementMethod(new ScrollingMovementMethod());
		textview1.setTypeface(Utility.font_reg);
		//textview1.setText(Html.fromHtml("<h4>  Your selected offer...  </h4>&#9733; You will  now see the offer  details, <br> &nbsp&nbsp&nbsp&nbsp including the address, contact details of <br>&nbsp&nbsp&nbsp&nbsp the  merchant, and all terms and <br> &nbsp&nbsp&nbsp&nbsp conditions.<br> <br>&#9733;   If you like the offer add it to \"your <br>&nbsp&nbsp&nbsp&nbsp favourites\" by clicking the heart. <br><br><br><br><br><br><br><br><br><br><br>"));
		textview1.setText(Html.fromHtml("<h4>  Your selected offer...  </h4>&#9733; You will  now see the offer  details, including the address, contact details of the  merchant, and all terms and conditions.<br> <br>&#9733;   If you like the offer add it to \"your favourites\" by clicking the heart. <br><br><br><br><br><br><br><br>"));
		return v;
	}
}
