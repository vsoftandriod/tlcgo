package com.easyapps.tlc.go.ctrl.model;

public class ProductDetails {
	private int id;
	private String name;
	private String highlight;
	private int merchant_id;
	private String details;
	private String text;
	private String terms_and_conditions;
 	private String link1;
	private String link2;
	private String link2_text;
	private String image_extension;
	private String display_image;
	private String mname;
	private String contact_first_name;
	private String contatct_last_name;
	private String contact_title;
	private String contact_position;
	private String mail_address1;
	private String mail_address2;
	private String mail_suburb;
	private String mail_state;
	private String mail_postcode;
	private String mail_country;
	private String email;
	private String phone;
	private String mobile;
	private String fax;
	private String latitude;
	private String longitude;
	private String logo_extension;
	private String m_image_extension;
	private String mtext;
	private String created;
	private String modified;
	private int mobile_reward_value;
	private int quantity;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHighlight() {
		return highlight;
	}
	public void setHighlight(String highlight) {
		this.highlight = highlight;
	}
	public int getMerchant_id() {
		return merchant_id;
	}
	public void setMerchant_id(int merchant_id) {
		this.merchant_id = merchant_id;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getTerms_and_conditions() {
		return terms_and_conditions;
	}
	public void setTerms_and_conditions(String terms_and_conditions) {
		this.terms_and_conditions = terms_and_conditions;
	}
	public String getLink1() {
		return link1;
	}
	public void setLink1(String link1) {
		this.link1 = link1;
	}
	public String getLink2() {
		return link2;
	}
	public void setLink2(String link2) {
		this.link2 = link2;
	}
	public String getLink2_text() {
		return link2_text;
	}
	public void setLink2_text(String link2_text) {
		this.link2_text = link2_text;
	}
	public String getImage_extension() {
		return image_extension;
	}
	public void setImage_extension(String image_extension) {
		this.image_extension = image_extension;
	}
	public String getDisplay_image() {
		return display_image;
	}
	public void setDisplay_image(String display_image) {
		this.display_image = display_image;
	}
	public String getMname() {
		return mname;
	}
	public void setMname(String mname) {
		this.mname = mname;
	}
	public String getContact_first_name() {
		return contact_first_name;
	}
	public void setContact_first_name(String contact_first_name) {
		this.contact_first_name = contact_first_name;
	}
	public String getContatct_last_name() {
		return contatct_last_name;
	}
	public void setContatct_last_name(String contatct_last_name) {
		this.contatct_last_name = contatct_last_name;
	}
	public String getContact_title() {
		return contact_title;
	}
	public void setContact_title(String contact_title) {
		this.contact_title = contact_title;
	}
	public String getContact_position() {
		return contact_position;
	}
	public void setContact_position(String contact_position) {
		this.contact_position = contact_position;
	}
	public String getMail_address1() {
		return mail_address1;
	}
	public void setMail_address1(String mail_address1) {
		this.mail_address1 = mail_address1;
	}
	public String getMail_address2() {
		return mail_address2;
	}
	public void setMail_address2(String mail_address2) {
		this.mail_address2 = mail_address2;
	}
	public String getMail_suburb() {
		return mail_suburb;
	}
	public void setMail_suburb(String mail_suburb) {
		this.mail_suburb = mail_suburb;
	}
	public String getMail_state() {
		return mail_state;
	}
	public void setMail_state(String mail_state) {
		this.mail_state = mail_state;
	}
	public String getMail_postcode() {
		return mail_postcode;
	}
	public void setMail_postcode(String mail_postcode) {
		this.mail_postcode = mail_postcode;
	}
	public String getMail_country() {
		return mail_country;
	}
	public void setMail_country(String mail_country) {
		this.mail_country = mail_country;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLogo_extension() {
		return logo_extension;
	}
	public void setLogo_extension(String logo_extension) {
		this.logo_extension = logo_extension;
	}
	public String getM_image_extension() {
		return m_image_extension;
	}
	public void setM_image_extension(String m_image_extension) {
		this.m_image_extension = m_image_extension;
	}
	public String getMtext() {
		return mtext;
	}
	public void setMtext(String mtext) {
		this.mtext = mtext;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getModified() {
		return modified;
	}
	public void setModified(String modified) {
		this.modified = modified;
	}
	public int getMobile_Reward() {
		return mobile_reward_value;
	}
	public void setMobile_Reward(int mobile_reward_value) {
		this.mobile_reward_value = mobile_reward_value;
	}
	public void setQuantity(int quantity) {
		this.quantity=quantity;
		
	}
	public int getQuantity() {
		return quantity;
	}
	
}
